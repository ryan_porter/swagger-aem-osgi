/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqScreensDeviceImplDevice
 */
public class OASComAdobeCqScreensDeviceImplDevice implements OAS.MappedProperties {
    /**
     * Get comAdobeAemScreensPlayerPingfrequency
     * @return comAdobeAemScreensPlayerPingfrequency
     */
    public OASConfigNodePropertyInteger comAdobeAemScreensPlayerPingfrequency { get; set; }

    /**
     * Get comAdobeAemScreensDevicePaswordSpecialchars
     * @return comAdobeAemScreensDevicePaswordSpecialchars
     */
    public OASConfigNodePropertyString comAdobeAemScreensDevicePaswordSpecialchars { get; set; }

    /**
     * Get comAdobeAemScreensDevicePaswordMinlowercasechars
     * @return comAdobeAemScreensDevicePaswordMinlowercasechars
     */
    public OASConfigNodePropertyInteger comAdobeAemScreensDevicePaswordMinlowercasechars { get; set; }

    /**
     * Get comAdobeAemScreensDevicePaswordMinuppercasechars
     * @return comAdobeAemScreensDevicePaswordMinuppercasechars
     */
    public OASConfigNodePropertyInteger comAdobeAemScreensDevicePaswordMinuppercasechars { get; set; }

    /**
     * Get comAdobeAemScreensDevicePaswordMinnumberchars
     * @return comAdobeAemScreensDevicePaswordMinnumberchars
     */
    public OASConfigNodePropertyInteger comAdobeAemScreensDevicePaswordMinnumberchars { get; set; }

    /**
     * Get comAdobeAemScreensDevicePaswordMinspecialchars
     * @return comAdobeAemScreensDevicePaswordMinspecialchars
     */
    public OASConfigNodePropertyInteger comAdobeAemScreensDevicePaswordMinspecialchars { get; set; }

    /**
     * Get comAdobeAemScreensDevicePaswordMinlength
     * @return comAdobeAemScreensDevicePaswordMinlength
     */
    public OASConfigNodePropertyInteger comAdobeAemScreensDevicePaswordMinlength { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'com.adobe.aem.screens.player.pingfrequency' => 'comAdobeAemScreensPlayerPingfrequency',
        'com.adobe.aem.screens.device.pasword.specialchars' => 'comAdobeAemScreensDevicePaswordSpecialchars',
        'com.adobe.aem.screens.device.pasword.minlowercasechars' => 'comAdobeAemScreensDevicePaswordMinlowercasechars',
        'com.adobe.aem.screens.device.pasword.minuppercasechars' => 'comAdobeAemScreensDevicePaswordMinuppercasechars',
        'com.adobe.aem.screens.device.pasword.minnumberchars' => 'comAdobeAemScreensDevicePaswordMinnumberchars',
        'com.adobe.aem.screens.device.pasword.minspecialchars' => 'comAdobeAemScreensDevicePaswordMinspecialchars',
        'com.adobe.aem.screens.device.pasword.minlength' => 'comAdobeAemScreensDevicePaswordMinlength'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqScreensDeviceImplDevice getExample() {
        OASComAdobeCqScreensDeviceImplDevice comAdobeCqScreensDeviceImplDeviceServiceProperties = new OASComAdobeCqScreensDeviceImplDevice();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensPlayerPingfrequency = OASConfigNodePropertyInteger.getExample();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordSpecialchars = OASConfigNodePropertyString.getExample();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinlowercasechars = OASConfigNodePropertyInteger.getExample();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinuppercasechars = OASConfigNodePropertyInteger.getExample();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinnumberchars = OASConfigNodePropertyInteger.getExample();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinspecialchars = OASConfigNodePropertyInteger.getExample();
          comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinlength = OASConfigNodePropertyInteger.getExample();
        return comAdobeCqScreensDeviceImplDeviceServiceProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqScreensDeviceImplDevice) {           
            OASComAdobeCqScreensDeviceImplDevice comAdobeCqScreensDeviceImplDeviceServiceProperties = (OASComAdobeCqScreensDeviceImplDevice) obj;
            return this.comAdobeAemScreensPlayerPingfrequency == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensPlayerPingfrequency
                && this.comAdobeAemScreensDevicePaswordSpecialchars == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordSpecialchars
                && this.comAdobeAemScreensDevicePaswordMinlowercasechars == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinlowercasechars
                && this.comAdobeAemScreensDevicePaswordMinuppercasechars == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinuppercasechars
                && this.comAdobeAemScreensDevicePaswordMinnumberchars == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinnumberchars
                && this.comAdobeAemScreensDevicePaswordMinspecialchars == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinspecialchars
                && this.comAdobeAemScreensDevicePaswordMinlength == comAdobeCqScreensDeviceImplDeviceServiceProperties.comAdobeAemScreensDevicePaswordMinlength;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (comAdobeAemScreensPlayerPingfrequency == null ? 0 : System.hashCode(comAdobeAemScreensPlayerPingfrequency));
        hashCode = (17 * hashCode) + (comAdobeAemScreensDevicePaswordSpecialchars == null ? 0 : System.hashCode(comAdobeAemScreensDevicePaswordSpecialchars));
        hashCode = (17 * hashCode) + (comAdobeAemScreensDevicePaswordMinlowercasechars == null ? 0 : System.hashCode(comAdobeAemScreensDevicePaswordMinlowercasechars));
        hashCode = (17 * hashCode) + (comAdobeAemScreensDevicePaswordMinuppercasechars == null ? 0 : System.hashCode(comAdobeAemScreensDevicePaswordMinuppercasechars));
        hashCode = (17 * hashCode) + (comAdobeAemScreensDevicePaswordMinnumberchars == null ? 0 : System.hashCode(comAdobeAemScreensDevicePaswordMinnumberchars));
        hashCode = (17 * hashCode) + (comAdobeAemScreensDevicePaswordMinspecialchars == null ? 0 : System.hashCode(comAdobeAemScreensDevicePaswordMinspecialchars));
        hashCode = (17 * hashCode) + (comAdobeAemScreensDevicePaswordMinlength == null ? 0 : System.hashCode(comAdobeAemScreensDevicePaswordMinlength));
        return hashCode;
    }
}

