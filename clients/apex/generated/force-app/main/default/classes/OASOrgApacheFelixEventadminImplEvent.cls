/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheFelixEventadminImplEvent
 */
public class OASOrgApacheFelixEventadminImplEvent implements OAS.MappedProperties {
    /**
     * Get orgApacheFelixEventadminThreadPoolSize
     * @return orgApacheFelixEventadminThreadPoolSize
     */
    public OASConfigNodePropertyInteger orgApacheFelixEventadminThreadPoolSize { get; set; }

    /**
     * Get orgApacheFelixEventadminAsyncToSyncThreadRatio
     * @return orgApacheFelixEventadminAsyncToSyncThreadRatio
     */
    public OASConfigNodePropertyFloat orgApacheFelixEventadminAsyncToSyncThreadRatio { get; set; }

    /**
     * Get orgApacheFelixEventadminTimeout
     * @return orgApacheFelixEventadminTimeout
     */
    public OASConfigNodePropertyInteger orgApacheFelixEventadminTimeout { get; set; }

    /**
     * Get orgApacheFelixEventadminRequireTopic
     * @return orgApacheFelixEventadminRequireTopic
     */
    public OASConfigNodePropertyBoolean orgApacheFelixEventadminRequireTopic { get; set; }

    /**
     * Get orgApacheFelixEventadminIgnoreTimeout
     * @return orgApacheFelixEventadminIgnoreTimeout
     */
    public OASConfigNodePropertyArray orgApacheFelixEventadminIgnoreTimeout { get; set; }

    /**
     * Get orgApacheFelixEventadminIgnoreTopic
     * @return orgApacheFelixEventadminIgnoreTopic
     */
    public OASConfigNodePropertyArray orgApacheFelixEventadminIgnoreTopic { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'org.apache.felix.eventadmin.ThreadPoolSize' => 'orgApacheFelixEventadminThreadPoolSize',
        'org.apache.felix.eventadmin.AsyncToSyncThreadRatio' => 'orgApacheFelixEventadminAsyncToSyncThreadRatio',
        'org.apache.felix.eventadmin.Timeout' => 'orgApacheFelixEventadminTimeout',
        'org.apache.felix.eventadmin.RequireTopic' => 'orgApacheFelixEventadminRequireTopic',
        'org.apache.felix.eventadmin.IgnoreTimeout' => 'orgApacheFelixEventadminIgnoreTimeout',
        'org.apache.felix.eventadmin.IgnoreTopic' => 'orgApacheFelixEventadminIgnoreTopic'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheFelixEventadminImplEvent getExample() {
        OASOrgApacheFelixEventadminImplEvent orgApacheFelixEventadminImplEventAdminProperties = new OASOrgApacheFelixEventadminImplEvent();
          orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminThreadPoolSize = OASConfigNodePropertyInteger.getExample();
          orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminAsyncToSyncThreadRatio = OASConfigNodePropertyFloat.getExample();
          orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminTimeout = OASConfigNodePropertyInteger.getExample();
          orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminRequireTopic = OASConfigNodePropertyBoolean.getExample();
          orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminIgnoreTimeout = OASConfigNodePropertyArray.getExample();
          orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminIgnoreTopic = OASConfigNodePropertyArray.getExample();
        return orgApacheFelixEventadminImplEventAdminProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheFelixEventadminImplEvent) {           
            OASOrgApacheFelixEventadminImplEvent orgApacheFelixEventadminImplEventAdminProperties = (OASOrgApacheFelixEventadminImplEvent) obj;
            return this.orgApacheFelixEventadminThreadPoolSize == orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminThreadPoolSize
                && this.orgApacheFelixEventadminAsyncToSyncThreadRatio == orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminAsyncToSyncThreadRatio
                && this.orgApacheFelixEventadminTimeout == orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminTimeout
                && this.orgApacheFelixEventadminRequireTopic == orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminRequireTopic
                && this.orgApacheFelixEventadminIgnoreTimeout == orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminIgnoreTimeout
                && this.orgApacheFelixEventadminIgnoreTopic == orgApacheFelixEventadminImplEventAdminProperties.orgApacheFelixEventadminIgnoreTopic;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (orgApacheFelixEventadminThreadPoolSize == null ? 0 : System.hashCode(orgApacheFelixEventadminThreadPoolSize));
        hashCode = (17 * hashCode) + (orgApacheFelixEventadminAsyncToSyncThreadRatio == null ? 0 : System.hashCode(orgApacheFelixEventadminAsyncToSyncThreadRatio));
        hashCode = (17 * hashCode) + (orgApacheFelixEventadminTimeout == null ? 0 : System.hashCode(orgApacheFelixEventadminTimeout));
        hashCode = (17 * hashCode) + (orgApacheFelixEventadminRequireTopic == null ? 0 : System.hashCode(orgApacheFelixEventadminRequireTopic));
        hashCode = (17 * hashCode) + (orgApacheFelixEventadminIgnoreTimeout == null ? 0 : System.hashCode(orgApacheFelixEventadminIgnoreTimeout));
        hashCode = (17 * hashCode) + (orgApacheFelixEventadminIgnoreTopic == null ? 0 : System.hashCode(orgApacheFelixEventadminIgnoreTopic));
        return hashCode;
    }
}

