/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqSocialConnectOauthImplS
 */
public class OASComAdobeCqSocialConnectOauthImplS implements OAS.MappedProperties {
    /**
     * Get facebook
     * @return facebook
     */
    public OASConfigNodePropertyArray facebook { get; set; }

    /**
     * Get twitter
     * @return twitter
     */
    public OASConfigNodePropertyArray twitter { get; set; }

    /**
     * Get providerConfigUserFolder
     * @return providerConfigUserFolder
     */
    public OASConfigNodePropertyString providerConfigUserFolder { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'provider.config.user.folder' => 'providerConfigUserFolder'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqSocialConnectOauthImplS getExample() {
        OASComAdobeCqSocialConnectOauthImplS comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties = new OASComAdobeCqSocialConnectOauthImplS();
          comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties.facebook = OASConfigNodePropertyArray.getExample();
          comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties.twitter = OASConfigNodePropertyArray.getExample();
          comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties.providerConfigUserFolder = OASConfigNodePropertyString.getExample();
        return comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqSocialConnectOauthImplS) {           
            OASComAdobeCqSocialConnectOauthImplS comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties = (OASComAdobeCqSocialConnectOauthImplS) obj;
            return this.facebook == comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties.facebook
                && this.twitter == comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties.twitter
                && this.providerConfigUserFolder == comAdobeCqSocialConnectOauthImplSocialOAuthUserProfileMapperProperties.providerConfigUserFolder;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (facebook == null ? 0 : System.hashCode(facebook));
        hashCode = (17 * hashCode) + (twitter == null ? 0 : System.hashCode(twitter));
        hashCode = (17 * hashCode) + (providerConfigUserFolder == null ? 0 : System.hashCode(providerConfigUserFolder));
        return hashCode;
    }
}

