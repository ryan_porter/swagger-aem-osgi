/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheSlingDistributionResourc
 */
public class OASOrgApacheSlingDistributionResourc implements OAS.MappedProperties {
    /**
     * Get providerRoots
     * @return providerRoots
     */
    public OASConfigNodePropertyString providerRoots { get; set; }

    /**
     * Get kind
     * @return kind
     */
    public OASConfigNodePropertyString kind { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'provider.roots' => 'providerRoots'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheSlingDistributionResourc getExample() {
        OASOrgApacheSlingDistributionResourc orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties = new OASOrgApacheSlingDistributionResourc();
          orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties.providerRoots = OASConfigNodePropertyString.getExample();
          orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties.kind = OASConfigNodePropertyString.getExample();
        return orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheSlingDistributionResourc) {           
            OASOrgApacheSlingDistributionResourc orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties = (OASOrgApacheSlingDistributionResourc) obj;
            return this.providerRoots == orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties.providerRoots
                && this.kind == orgApacheSlingDistributionResourcesImplDistributionServiceResourProperties.kind;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (providerRoots == null ? 0 : System.hashCode(providerRoots));
        hashCode = (17 * hashCode) + (kind == null ? 0 : System.hashCode(kind));
        return hashCode;
    }
}

