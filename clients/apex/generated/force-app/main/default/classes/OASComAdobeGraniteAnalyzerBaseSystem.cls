/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteAnalyzerBaseSystem
 */
public class OASComAdobeGraniteAnalyzerBaseSystem {
    /**
     * Get disabled
     * @return disabled
     */
    public OASConfigNodePropertyBoolean disabled { get; set; }

    public static OASComAdobeGraniteAnalyzerBaseSystem getExample() {
        OASComAdobeGraniteAnalyzerBaseSystem comAdobeGraniteAnalyzerBaseSystemStatusServletProperties = new OASComAdobeGraniteAnalyzerBaseSystem();
          comAdobeGraniteAnalyzerBaseSystemStatusServletProperties.disabled = OASConfigNodePropertyBoolean.getExample();
        return comAdobeGraniteAnalyzerBaseSystemStatusServletProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteAnalyzerBaseSystem) {           
            OASComAdobeGraniteAnalyzerBaseSystem comAdobeGraniteAnalyzerBaseSystemStatusServletProperties = (OASComAdobeGraniteAnalyzerBaseSystem) obj;
            return this.disabled == comAdobeGraniteAnalyzerBaseSystemStatusServletProperties.disabled;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (disabled == null ? 0 : System.hashCode(disabled));
        return hashCode;
    }
}

