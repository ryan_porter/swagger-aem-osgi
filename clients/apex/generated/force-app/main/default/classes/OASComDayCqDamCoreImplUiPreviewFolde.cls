/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamCoreImplUiPreviewFolde
 */
public class OASComDayCqDamCoreImplUiPreviewFolde {
    /**
     * Get createPreviewEnabled
     * @return createPreviewEnabled
     */
    public OASConfigNodePropertyBoolean createPreviewEnabled { get; set; }

    /**
     * Get updatePreviewEnabled
     * @return updatePreviewEnabled
     */
    public OASConfigNodePropertyBoolean updatePreviewEnabled { get; set; }

    /**
     * Get queueSize
     * @return queueSize
     */
    public OASConfigNodePropertyInteger queueSize { get; set; }

    /**
     * Get folderPreviewRenditionRegex
     * @return folderPreviewRenditionRegex
     */
    public OASConfigNodePropertyString folderPreviewRenditionRegex { get; set; }

    public static OASComDayCqDamCoreImplUiPreviewFolde getExample() {
        OASComDayCqDamCoreImplUiPreviewFolde comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties = new OASComDayCqDamCoreImplUiPreviewFolde();
          comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.createPreviewEnabled = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.updatePreviewEnabled = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.queueSize = OASConfigNodePropertyInteger.getExample();
          comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.folderPreviewRenditionRegex = OASConfigNodePropertyString.getExample();
        return comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamCoreImplUiPreviewFolde) {           
            OASComDayCqDamCoreImplUiPreviewFolde comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties = (OASComDayCqDamCoreImplUiPreviewFolde) obj;
            return this.createPreviewEnabled == comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.createPreviewEnabled
                && this.updatePreviewEnabled == comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.updatePreviewEnabled
                && this.queueSize == comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.queueSize
                && this.folderPreviewRenditionRegex == comDayCqDamCoreImplUiPreviewFolderPreviewUpdaterImplProperties.folderPreviewRenditionRegex;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (createPreviewEnabled == null ? 0 : System.hashCode(createPreviewEnabled));
        hashCode = (17 * hashCode) + (updatePreviewEnabled == null ? 0 : System.hashCode(updatePreviewEnabled));
        hashCode = (17 * hashCode) + (queueSize == null ? 0 : System.hashCode(queueSize));
        hashCode = (17 * hashCode) + (folderPreviewRenditionRegex == null ? 0 : System.hashCode(folderPreviewRenditionRegex));
        return hashCode;
    }
}

