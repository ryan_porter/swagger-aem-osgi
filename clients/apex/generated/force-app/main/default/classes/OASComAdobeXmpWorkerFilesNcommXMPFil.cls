/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeXmpWorkerFilesNcommXMPFil
 */
public class OASComAdobeXmpWorkerFilesNcommXMPFil {
    /**
     * Get maxConnections
     * @return maxConnections
     */
    public OASConfigNodePropertyString maxConnections { get; set; }

    /**
     * Get maxRequests
     * @return maxRequests
     */
    public OASConfigNodePropertyString maxRequests { get; set; }

    /**
     * Get requestTimeout
     * @return requestTimeout
     */
    public OASConfigNodePropertyString requestTimeout { get; set; }

    /**
     * Get logDir
     * @return logDir
     */
    public OASConfigNodePropertyString logDir { get; set; }

    public static OASComAdobeXmpWorkerFilesNcommXMPFil getExample() {
        OASComAdobeXmpWorkerFilesNcommXMPFil comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties = new OASComAdobeXmpWorkerFilesNcommXMPFil();
          comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.maxConnections = OASConfigNodePropertyString.getExample();
          comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.maxRequests = OASConfigNodePropertyString.getExample();
          comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.requestTimeout = OASConfigNodePropertyString.getExample();
          comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.logDir = OASConfigNodePropertyString.getExample();
        return comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeXmpWorkerFilesNcommXMPFil) {           
            OASComAdobeXmpWorkerFilesNcommXMPFil comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties = (OASComAdobeXmpWorkerFilesNcommXMPFil) obj;
            return this.maxConnections == comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.maxConnections
                && this.maxRequests == comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.maxRequests
                && this.requestTimeout == comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.requestTimeout
                && this.logDir == comAdobeXmpWorkerFilesNcommXMPFilesNCommProperties.logDir;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (maxConnections == null ? 0 : System.hashCode(maxConnections));
        hashCode = (17 * hashCode) + (maxRequests == null ? 0 : System.hashCode(maxRequests));
        hashCode = (17 * hashCode) + (requestTimeout == null ? 0 : System.hashCode(requestTimeout));
        hashCode = (17 * hashCode) + (logDir == null ? 0 : System.hashCode(logDir));
        return hashCode;
    }
}

