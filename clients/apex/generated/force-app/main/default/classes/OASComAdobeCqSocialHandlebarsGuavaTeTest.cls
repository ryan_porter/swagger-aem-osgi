@isTest
private class OASComAdobeCqSocialHandlebarsGuavaTeTest {
    @isTest
    private static void equalsSameInstance() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2 = comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1;
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3 = new OASComAdobeCqSocialHandlebarsGuavaTe();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4 = comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3;

        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3 = new OASComAdobeCqSocialHandlebarsGuavaTe();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4 = new OASComAdobeCqSocialHandlebarsGuavaTe();

        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2 = new OASComAdobeCqSocialHandlebarsGuavaTe();

        System.assertEquals(false, comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.equals('foo'));
        System.assertEquals(false, comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2 = new OASComAdobeCqSocialHandlebarsGuavaTe();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3;

        System.assertEquals(false, comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3));
        System.assertEquals(false, comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2 = new OASComAdobeCqSocialHandlebarsGuavaTe();

        System.assertEquals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.hashCode(), comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.hashCode());
        System.assertEquals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.hashCode(), comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2 = OASComAdobeCqSocialHandlebarsGuavaTe.getExample();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3 = new OASComAdobeCqSocialHandlebarsGuavaTe();
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4 = new OASComAdobeCqSocialHandlebarsGuavaTe();

        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2));
        System.assert(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3.equals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4));
        System.assertEquals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties1.hashCode(), comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties2.hashCode());
        System.assertEquals(comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties3.hashCode(), comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties4.hashCode());
    }

    @isTest
    private static void maintainRenamedProperties() {
        OASComAdobeCqSocialHandlebarsGuavaTe comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties = new OASComAdobeCqSocialHandlebarsGuavaTe();
        Map<String, String> propertyMappings = comAdobeCqSocialHandlebarsGuavaTemplateCacheImplProperties.getPropertyMappings();
        System.assertEquals('parameterGuavaCacheEnabled', propertyMappings.get('parameter.guava.cache.enabled'));
        System.assertEquals('parameterGuavaCacheParams', propertyMappings.get('parameter.guava.cache.params'));
        System.assertEquals('parameterGuavaCacheReload', propertyMappings.get('parameter.guava.cache.reload'));
        System.assertEquals('serviceRanking', propertyMappings.get('service.ranking'));
    }
}
