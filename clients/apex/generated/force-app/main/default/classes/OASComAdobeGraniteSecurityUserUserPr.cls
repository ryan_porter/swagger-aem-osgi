/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteSecurityUserUserPr
 */
public class OASComAdobeGraniteSecurityUserUserPr implements OAS.MappedProperties {
    /**
     * Get adapterCondition
     * @return adapterCondition
     */
    public OASConfigNodePropertyString adapterCondition { get; set; }

    /**
     * Get graniteUserpropertiesNodetypes
     * @return graniteUserpropertiesNodetypes
     */
    public OASConfigNodePropertyArray graniteUserpropertiesNodetypes { get; set; }

    /**
     * Get graniteUserpropertiesResourcetypes
     * @return graniteUserpropertiesResourcetypes
     */
    public OASConfigNodePropertyArray graniteUserpropertiesResourcetypes { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'adapter.condition' => 'adapterCondition',
        'granite.userproperties.nodetypes' => 'graniteUserpropertiesNodetypes',
        'granite.userproperties.resourcetypes' => 'graniteUserpropertiesResourcetypes'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeGraniteSecurityUserUserPr getExample() {
        OASComAdobeGraniteSecurityUserUserPr comAdobeGraniteSecurityUserUserPropertiesServiceProperties = new OASComAdobeGraniteSecurityUserUserPr();
          comAdobeGraniteSecurityUserUserPropertiesServiceProperties.adapterCondition = OASConfigNodePropertyString.getExample();
          comAdobeGraniteSecurityUserUserPropertiesServiceProperties.graniteUserpropertiesNodetypes = OASConfigNodePropertyArray.getExample();
          comAdobeGraniteSecurityUserUserPropertiesServiceProperties.graniteUserpropertiesResourcetypes = OASConfigNodePropertyArray.getExample();
        return comAdobeGraniteSecurityUserUserPropertiesServiceProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteSecurityUserUserPr) {           
            OASComAdobeGraniteSecurityUserUserPr comAdobeGraniteSecurityUserUserPropertiesServiceProperties = (OASComAdobeGraniteSecurityUserUserPr) obj;
            return this.adapterCondition == comAdobeGraniteSecurityUserUserPropertiesServiceProperties.adapterCondition
                && this.graniteUserpropertiesNodetypes == comAdobeGraniteSecurityUserUserPropertiesServiceProperties.graniteUserpropertiesNodetypes
                && this.graniteUserpropertiesResourcetypes == comAdobeGraniteSecurityUserUserPropertiesServiceProperties.graniteUserpropertiesResourcetypes;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (adapterCondition == null ? 0 : System.hashCode(adapterCondition));
        hashCode = (17 * hashCode) + (graniteUserpropertiesNodetypes == null ? 0 : System.hashCode(graniteUserpropertiesNodetypes));
        hashCode = (17 * hashCode) + (graniteUserpropertiesResourcetypes == null ? 0 : System.hashCode(graniteUserpropertiesResourcetypes));
        return hashCode;
    }
}

