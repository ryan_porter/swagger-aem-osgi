/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheFelixJaasConfigurationSp
 */
public class OASOrgApacheFelixJaasConfigurationSp implements OAS.MappedProperties {
    /**
     * Get jaasDefaultRealmName
     * @return jaasDefaultRealmName
     */
    public OASConfigNodePropertyString jaasDefaultRealmName { get; set; }

    /**
     * Get jaasConfigProviderName
     * @return jaasConfigProviderName
     */
    public OASConfigNodePropertyString jaasConfigProviderName { get; set; }

    /**
     * Get jaasGlobalConfigPolicy
     * @return jaasGlobalConfigPolicy
     */
    public OASConfigNodePropertyDropDown jaasGlobalConfigPolicy { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'jaas.defaultRealmName' => 'jaasDefaultRealmName',
        'jaas.configProviderName' => 'jaasConfigProviderName',
        'jaas.globalConfigPolicy' => 'jaasGlobalConfigPolicy'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheFelixJaasConfigurationSp getExample() {
        OASOrgApacheFelixJaasConfigurationSp orgApacheFelixJaasConfigurationSpiProperties = new OASOrgApacheFelixJaasConfigurationSp();
          orgApacheFelixJaasConfigurationSpiProperties.jaasDefaultRealmName = OASConfigNodePropertyString.getExample();
          orgApacheFelixJaasConfigurationSpiProperties.jaasConfigProviderName = OASConfigNodePropertyString.getExample();
          orgApacheFelixJaasConfigurationSpiProperties.jaasGlobalConfigPolicy = OASConfigNodePropertyDropDown.getExample();
        return orgApacheFelixJaasConfigurationSpiProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheFelixJaasConfigurationSp) {           
            OASOrgApacheFelixJaasConfigurationSp orgApacheFelixJaasConfigurationSpiProperties = (OASOrgApacheFelixJaasConfigurationSp) obj;
            return this.jaasDefaultRealmName == orgApacheFelixJaasConfigurationSpiProperties.jaasDefaultRealmName
                && this.jaasConfigProviderName == orgApacheFelixJaasConfigurationSpiProperties.jaasConfigProviderName
                && this.jaasGlobalConfigPolicy == orgApacheFelixJaasConfigurationSpiProperties.jaasGlobalConfigPolicy;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (jaasDefaultRealmName == null ? 0 : System.hashCode(jaasDefaultRealmName));
        hashCode = (17 * hashCode) + (jaasConfigProviderName == null ? 0 : System.hashCode(jaasConfigProviderName));
        hashCode = (17 * hashCode) + (jaasGlobalConfigPolicy == null ? 0 : System.hashCode(jaasGlobalConfigPolicy));
        return hashCode;
    }
}

