/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASApacheSlingHealthCheckResultHTMLS
 */
public class OASApacheSlingHealthCheckResultHTMLS {
    /**
     * Get styleString
     * @return styleString
     */
    public OASConfigNodePropertyString styleString { get; set; }

    public static OASApacheSlingHealthCheckResultHTMLS getExample() {
        OASApacheSlingHealthCheckResultHTMLS apacheSlingHealthCheckResultHTMLSerializerProperties = new OASApacheSlingHealthCheckResultHTMLS();
          apacheSlingHealthCheckResultHTMLSerializerProperties.styleString = OASConfigNodePropertyString.getExample();
        return apacheSlingHealthCheckResultHTMLSerializerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASApacheSlingHealthCheckResultHTMLS) {           
            OASApacheSlingHealthCheckResultHTMLS apacheSlingHealthCheckResultHTMLSerializerProperties = (OASApacheSlingHealthCheckResultHTMLS) obj;
            return this.styleString == apacheSlingHealthCheckResultHTMLSerializerProperties.styleString;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (styleString == null ? 0 : System.hashCode(styleString));
        return hashCode;
    }
}

