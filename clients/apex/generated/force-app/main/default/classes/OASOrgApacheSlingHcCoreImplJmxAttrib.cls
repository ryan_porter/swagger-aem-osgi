/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheSlingHcCoreImplJmxAttrib
 */
public class OASOrgApacheSlingHcCoreImplJmxAttrib implements OAS.MappedProperties {
    /**
     * Get hcName
     * @return hcName
     */
    public OASConfigNodePropertyString hcName { get; set; }

    /**
     * Get hcTags
     * @return hcTags
     */
    public OASConfigNodePropertyArray hcTags { get; set; }

    /**
     * Get hcMbeanName
     * @return hcMbeanName
     */
    public OASConfigNodePropertyString hcMbeanName { get; set; }

    /**
     * Get mbeanName
     * @return mbeanName
     */
    public OASConfigNodePropertyString mbeanName { get; set; }

    /**
     * Get attributeName
     * @return attributeName
     */
    public OASConfigNodePropertyString attributeName { get; set; }

    /**
     * Get attributeValueConstraint
     * @return attributeValueConstraint
     */
    public OASConfigNodePropertyString attributeValueConstraint { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'hc.name' => 'hcName',
        'hc.tags' => 'hcTags',
        'hc.mbean.name' => 'hcMbeanName',
        'mbean.name' => 'mbeanName',
        'attribute.name' => 'attributeName',
        'attribute.value.constraint' => 'attributeValueConstraint'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheSlingHcCoreImplJmxAttrib getExample() {
        OASOrgApacheSlingHcCoreImplJmxAttrib orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties = new OASOrgApacheSlingHcCoreImplJmxAttrib();
          orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.hcName = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.hcTags = OASConfigNodePropertyArray.getExample();
          orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.hcMbeanName = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.mbeanName = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.attributeName = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.attributeValueConstraint = OASConfigNodePropertyString.getExample();
        return orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheSlingHcCoreImplJmxAttrib) {           
            OASOrgApacheSlingHcCoreImplJmxAttrib orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties = (OASOrgApacheSlingHcCoreImplJmxAttrib) obj;
            return this.hcName == orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.hcName
                && this.hcTags == orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.hcTags
                && this.hcMbeanName == orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.hcMbeanName
                && this.mbeanName == orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.mbeanName
                && this.attributeName == orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.attributeName
                && this.attributeValueConstraint == orgApacheSlingHcCoreImplJmxAttributeHealthCheckProperties.attributeValueConstraint;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (hcName == null ? 0 : System.hashCode(hcName));
        hashCode = (17 * hashCode) + (hcTags == null ? 0 : System.hashCode(hcTags));
        hashCode = (17 * hashCode) + (hcMbeanName == null ? 0 : System.hashCode(hcMbeanName));
        hashCode = (17 * hashCode) + (mbeanName == null ? 0 : System.hashCode(mbeanName));
        hashCode = (17 * hashCode) + (attributeName == null ? 0 : System.hashCode(attributeName));
        hashCode = (17 * hashCode) + (attributeValueConstraint == null ? 0 : System.hashCode(attributeValueConstraint));
        return hashCode;
    }
}

