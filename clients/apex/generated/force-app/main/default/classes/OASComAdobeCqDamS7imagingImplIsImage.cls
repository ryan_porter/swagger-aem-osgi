/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqDamS7imagingImplIsImage
 */
public class OASComAdobeCqDamS7imagingImplIsImage implements OAS.MappedProperties {
    /**
     * Get tcpPort
     * @return tcpPort
     */
    public OASConfigNodePropertyString tcpPort { get; set; }

    /**
     * Get allowRemoteAccess
     * @return allowRemoteAccess
     */
    public OASConfigNodePropertyBoolean allowRemoteAccess { get; set; }

    /**
     * Get maxRenderRgnPixels
     * @return maxRenderRgnPixels
     */
    public OASConfigNodePropertyString maxRenderRgnPixels { get; set; }

    /**
     * Get maxMessageSize
     * @return maxMessageSize
     */
    public OASConfigNodePropertyString maxMessageSize { get; set; }

    /**
     * Get randomAccessUrlTimeout
     * @return randomAccessUrlTimeout
     */
    public OASConfigNodePropertyInteger randomAccessUrlTimeout { get; set; }

    /**
     * Get workerThreads
     * @return workerThreads
     */
    public OASConfigNodePropertyInteger workerThreads { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'TcpPort' => 'tcpPort',
        'AllowRemoteAccess' => 'allowRemoteAccess',
        'MaxRenderRgnPixels' => 'maxRenderRgnPixels',
        'MaxMessageSize' => 'maxMessageSize',
        'RandomAccessUrlTimeout' => 'randomAccessUrlTimeout',
        'WorkerThreads' => 'workerThreads'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqDamS7imagingImplIsImage getExample() {
        OASComAdobeCqDamS7imagingImplIsImage comAdobeCqDamS7imagingImplIsImageServerComponentProperties = new OASComAdobeCqDamS7imagingImplIsImage();
          comAdobeCqDamS7imagingImplIsImageServerComponentProperties.tcpPort = OASConfigNodePropertyString.getExample();
          comAdobeCqDamS7imagingImplIsImageServerComponentProperties.allowRemoteAccess = OASConfigNodePropertyBoolean.getExample();
          comAdobeCqDamS7imagingImplIsImageServerComponentProperties.maxRenderRgnPixels = OASConfigNodePropertyString.getExample();
          comAdobeCqDamS7imagingImplIsImageServerComponentProperties.maxMessageSize = OASConfigNodePropertyString.getExample();
          comAdobeCqDamS7imagingImplIsImageServerComponentProperties.randomAccessUrlTimeout = OASConfigNodePropertyInteger.getExample();
          comAdobeCqDamS7imagingImplIsImageServerComponentProperties.workerThreads = OASConfigNodePropertyInteger.getExample();
        return comAdobeCqDamS7imagingImplIsImageServerComponentProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqDamS7imagingImplIsImage) {           
            OASComAdobeCqDamS7imagingImplIsImage comAdobeCqDamS7imagingImplIsImageServerComponentProperties = (OASComAdobeCqDamS7imagingImplIsImage) obj;
            return this.tcpPort == comAdobeCqDamS7imagingImplIsImageServerComponentProperties.tcpPort
                && this.allowRemoteAccess == comAdobeCqDamS7imagingImplIsImageServerComponentProperties.allowRemoteAccess
                && this.maxRenderRgnPixels == comAdobeCqDamS7imagingImplIsImageServerComponentProperties.maxRenderRgnPixels
                && this.maxMessageSize == comAdobeCqDamS7imagingImplIsImageServerComponentProperties.maxMessageSize
                && this.randomAccessUrlTimeout == comAdobeCqDamS7imagingImplIsImageServerComponentProperties.randomAccessUrlTimeout
                && this.workerThreads == comAdobeCqDamS7imagingImplIsImageServerComponentProperties.workerThreads;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (tcpPort == null ? 0 : System.hashCode(tcpPort));
        hashCode = (17 * hashCode) + (allowRemoteAccess == null ? 0 : System.hashCode(allowRemoteAccess));
        hashCode = (17 * hashCode) + (maxRenderRgnPixels == null ? 0 : System.hashCode(maxRenderRgnPixels));
        hashCode = (17 * hashCode) + (maxMessageSize == null ? 0 : System.hashCode(maxMessageSize));
        hashCode = (17 * hashCode) + (randomAccessUrlTimeout == null ? 0 : System.hashCode(randomAccessUrlTimeout));
        hashCode = (17 * hashCode) + (workerThreads == null ? 0 : System.hashCode(workerThreads));
        return hashCode;
    }
}

