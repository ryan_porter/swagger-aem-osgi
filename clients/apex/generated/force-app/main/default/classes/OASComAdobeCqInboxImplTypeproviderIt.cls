/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqInboxImplTypeproviderIt
 */
public class OASComAdobeCqInboxImplTypeproviderIt implements OAS.MappedProperties {
    /**
     * Get inboxImplTypeproviderRegistrypaths
     * @return inboxImplTypeproviderRegistrypaths
     */
    public OASConfigNodePropertyArray inboxImplTypeproviderRegistrypaths { get; set; }

    /**
     * Get inboxImplTypeproviderLegacypaths
     * @return inboxImplTypeproviderLegacypaths
     */
    public OASConfigNodePropertyArray inboxImplTypeproviderLegacypaths { get; set; }

    /**
     * Get inboxImplTypeproviderDefaulturlFailureitem
     * @return inboxImplTypeproviderDefaulturlFailureitem
     */
    public OASConfigNodePropertyString inboxImplTypeproviderDefaulturlFailureitem { get; set; }

    /**
     * Get inboxImplTypeproviderDefaulturlWorkitem
     * @return inboxImplTypeproviderDefaulturlWorkitem
     */
    public OASConfigNodePropertyString inboxImplTypeproviderDefaulturlWorkitem { get; set; }

    /**
     * Get inboxImplTypeproviderDefaulturlTask
     * @return inboxImplTypeproviderDefaulturlTask
     */
    public OASConfigNodePropertyString inboxImplTypeproviderDefaulturlTask { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'inbox.impl.typeprovider.registrypaths' => 'inboxImplTypeproviderRegistrypaths',
        'inbox.impl.typeprovider.legacypaths' => 'inboxImplTypeproviderLegacypaths',
        'inbox.impl.typeprovider.defaulturl.failureitem' => 'inboxImplTypeproviderDefaulturlFailureitem',
        'inbox.impl.typeprovider.defaulturl.workitem' => 'inboxImplTypeproviderDefaulturlWorkitem',
        'inbox.impl.typeprovider.defaulturl.task' => 'inboxImplTypeproviderDefaulturlTask'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqInboxImplTypeproviderIt getExample() {
        OASComAdobeCqInboxImplTypeproviderIt comAdobeCqInboxImplTypeproviderItemTypeProviderProperties = new OASComAdobeCqInboxImplTypeproviderIt();
          comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderRegistrypaths = OASConfigNodePropertyArray.getExample();
          comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderLegacypaths = OASConfigNodePropertyArray.getExample();
          comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderDefaulturlFailureitem = OASConfigNodePropertyString.getExample();
          comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderDefaulturlWorkitem = OASConfigNodePropertyString.getExample();
          comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderDefaulturlTask = OASConfigNodePropertyString.getExample();
        return comAdobeCqInboxImplTypeproviderItemTypeProviderProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqInboxImplTypeproviderIt) {           
            OASComAdobeCqInboxImplTypeproviderIt comAdobeCqInboxImplTypeproviderItemTypeProviderProperties = (OASComAdobeCqInboxImplTypeproviderIt) obj;
            return this.inboxImplTypeproviderRegistrypaths == comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderRegistrypaths
                && this.inboxImplTypeproviderLegacypaths == comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderLegacypaths
                && this.inboxImplTypeproviderDefaulturlFailureitem == comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderDefaulturlFailureitem
                && this.inboxImplTypeproviderDefaulturlWorkitem == comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderDefaulturlWorkitem
                && this.inboxImplTypeproviderDefaulturlTask == comAdobeCqInboxImplTypeproviderItemTypeProviderProperties.inboxImplTypeproviderDefaulturlTask;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (inboxImplTypeproviderRegistrypaths == null ? 0 : System.hashCode(inboxImplTypeproviderRegistrypaths));
        hashCode = (17 * hashCode) + (inboxImplTypeproviderLegacypaths == null ? 0 : System.hashCode(inboxImplTypeproviderLegacypaths));
        hashCode = (17 * hashCode) + (inboxImplTypeproviderDefaulturlFailureitem == null ? 0 : System.hashCode(inboxImplTypeproviderDefaulturlFailureitem));
        hashCode = (17 * hashCode) + (inboxImplTypeproviderDefaulturlWorkitem == null ? 0 : System.hashCode(inboxImplTypeproviderDefaulturlWorkitem));
        hashCode = (17 * hashCode) + (inboxImplTypeproviderDefaulturlTask == null ? 0 : System.hashCode(inboxImplTypeproviderDefaulturlTask));
        return hashCode;
    }
}

