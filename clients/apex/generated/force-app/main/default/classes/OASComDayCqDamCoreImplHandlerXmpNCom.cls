/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamCoreImplHandlerXmpNCom
 */
public class OASComDayCqDamCoreImplHandlerXmpNCom implements OAS.MappedProperties {
    /**
     * Get xmphandlerCqFormats
     * @return xmphandlerCqFormats
     */
    public OASConfigNodePropertyArray xmphandlerCqFormats { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'xmphandler.cq.formats' => 'xmphandlerCqFormats'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamCoreImplHandlerXmpNCom getExample() {
        OASComDayCqDamCoreImplHandlerXmpNCom comDayCqDamCoreImplHandlerXmpNCommXMPHandlerProperties = new OASComDayCqDamCoreImplHandlerXmpNCom();
          comDayCqDamCoreImplHandlerXmpNCommXMPHandlerProperties.xmphandlerCqFormats = OASConfigNodePropertyArray.getExample();
        return comDayCqDamCoreImplHandlerXmpNCommXMPHandlerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamCoreImplHandlerXmpNCom) {           
            OASComDayCqDamCoreImplHandlerXmpNCom comDayCqDamCoreImplHandlerXmpNCommXMPHandlerProperties = (OASComDayCqDamCoreImplHandlerXmpNCom) obj;
            return this.xmphandlerCqFormats == comDayCqDamCoreImplHandlerXmpNCommXMPHandlerProperties.xmphandlerCqFormats;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (xmphandlerCqFormats == null ? 0 : System.hashCode(xmphandlerCqFormats));
        return hashCode;
    }
}

