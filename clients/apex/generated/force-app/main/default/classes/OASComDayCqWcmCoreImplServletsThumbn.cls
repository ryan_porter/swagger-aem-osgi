/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWcmCoreImplServletsThumbn
 */
public class OASComDayCqWcmCoreImplServletsThumbn {
    /**
     * Get workspace
     * @return workspace
     */
    public OASConfigNodePropertyString workspace { get; set; }

    /**
     * Get dimensions
     * @return dimensions
     */
    public OASConfigNodePropertyArray dimensions { get; set; }

    public static OASComDayCqWcmCoreImplServletsThumbn getExample() {
        OASComDayCqWcmCoreImplServletsThumbn comDayCqWcmCoreImplServletsThumbnailServletProperties = new OASComDayCqWcmCoreImplServletsThumbn();
          comDayCqWcmCoreImplServletsThumbnailServletProperties.workspace = OASConfigNodePropertyString.getExample();
          comDayCqWcmCoreImplServletsThumbnailServletProperties.dimensions = OASConfigNodePropertyArray.getExample();
        return comDayCqWcmCoreImplServletsThumbnailServletProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWcmCoreImplServletsThumbn) {           
            OASComDayCqWcmCoreImplServletsThumbn comDayCqWcmCoreImplServletsThumbnailServletProperties = (OASComDayCqWcmCoreImplServletsThumbn) obj;
            return this.workspace == comDayCqWcmCoreImplServletsThumbnailServletProperties.workspace
                && this.dimensions == comDayCqWcmCoreImplServletsThumbnailServletProperties.dimensions;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (workspace == null ? 0 : System.hashCode(workspace));
        hashCode = (17 * hashCode) + (dimensions == null ? 0 : System.hashCode(dimensions));
        return hashCode;
    }
}

