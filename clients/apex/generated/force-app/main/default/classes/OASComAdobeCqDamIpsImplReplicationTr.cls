/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqDamIpsImplReplicationTr
 */
public class OASComAdobeCqDamIpsImplReplicationTr implements OAS.MappedProperties {
    /**
     * Get dmreplicateonmodifyEnabled
     * @return dmreplicateonmodifyEnabled
     */
    public OASConfigNodePropertyBoolean dmreplicateonmodifyEnabled { get; set; }

    /**
     * Get dmreplicateonmodifyForcesyncdeletes
     * @return dmreplicateonmodifyForcesyncdeletes
     */
    public OASConfigNodePropertyBoolean dmreplicateonmodifyForcesyncdeletes { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'dmreplicateonmodify.enabled' => 'dmreplicateonmodifyEnabled',
        'dmreplicateonmodify.forcesyncdeletes' => 'dmreplicateonmodifyForcesyncdeletes'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqDamIpsImplReplicationTr getExample() {
        OASComAdobeCqDamIpsImplReplicationTr comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties = new OASComAdobeCqDamIpsImplReplicationTr();
          comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties.dmreplicateonmodifyEnabled = OASConfigNodePropertyBoolean.getExample();
          comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties.dmreplicateonmodifyForcesyncdeletes = OASConfigNodePropertyBoolean.getExample();
        return comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqDamIpsImplReplicationTr) {           
            OASComAdobeCqDamIpsImplReplicationTr comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties = (OASComAdobeCqDamIpsImplReplicationTr) obj;
            return this.dmreplicateonmodifyEnabled == comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties.dmreplicateonmodifyEnabled
                && this.dmreplicateonmodifyForcesyncdeletes == comAdobeCqDamIpsImplReplicationTriggerReplicateOnModifyWorkerProperties.dmreplicateonmodifyForcesyncdeletes;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (dmreplicateonmodifyEnabled == null ? 0 : System.hashCode(dmreplicateonmodifyEnabled));
        hashCode = (17 * hashCode) + (dmreplicateonmodifyForcesyncdeletes == null ? 0 : System.hashCode(dmreplicateonmodifyForcesyncdeletes));
        return hashCode;
    }
}

