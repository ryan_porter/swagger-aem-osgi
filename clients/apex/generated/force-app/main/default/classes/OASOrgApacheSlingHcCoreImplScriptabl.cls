/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheSlingHcCoreImplScriptabl
 */
public class OASOrgApacheSlingHcCoreImplScriptabl implements OAS.MappedProperties {
    /**
     * Get hcName
     * @return hcName
     */
    public OASConfigNodePropertyString hcName { get; set; }

    /**
     * Get hcTags
     * @return hcTags
     */
    public OASConfigNodePropertyArray hcTags { get; set; }

    /**
     * Get hcMbeanName
     * @return hcMbeanName
     */
    public OASConfigNodePropertyString hcMbeanName { get; set; }

    /**
     * Get expression
     * @return expression
     */
    public OASConfigNodePropertyString expression { get; set; }

    /**
     * Get languageExtension
     * @return languageExtension
     */
    public OASConfigNodePropertyString languageExtension { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'hc.name' => 'hcName',
        'hc.tags' => 'hcTags',
        'hc.mbean.name' => 'hcMbeanName',
        'language.extension' => 'languageExtension'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheSlingHcCoreImplScriptabl getExample() {
        OASOrgApacheSlingHcCoreImplScriptabl orgApacheSlingHcCoreImplScriptableHealthCheckProperties = new OASOrgApacheSlingHcCoreImplScriptabl();
          orgApacheSlingHcCoreImplScriptableHealthCheckProperties.hcName = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplScriptableHealthCheckProperties.hcTags = OASConfigNodePropertyArray.getExample();
          orgApacheSlingHcCoreImplScriptableHealthCheckProperties.hcMbeanName = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplScriptableHealthCheckProperties.expression = OASConfigNodePropertyString.getExample();
          orgApacheSlingHcCoreImplScriptableHealthCheckProperties.languageExtension = OASConfigNodePropertyString.getExample();
        return orgApacheSlingHcCoreImplScriptableHealthCheckProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheSlingHcCoreImplScriptabl) {           
            OASOrgApacheSlingHcCoreImplScriptabl orgApacheSlingHcCoreImplScriptableHealthCheckProperties = (OASOrgApacheSlingHcCoreImplScriptabl) obj;
            return this.hcName == orgApacheSlingHcCoreImplScriptableHealthCheckProperties.hcName
                && this.hcTags == orgApacheSlingHcCoreImplScriptableHealthCheckProperties.hcTags
                && this.hcMbeanName == orgApacheSlingHcCoreImplScriptableHealthCheckProperties.hcMbeanName
                && this.expression == orgApacheSlingHcCoreImplScriptableHealthCheckProperties.expression
                && this.languageExtension == orgApacheSlingHcCoreImplScriptableHealthCheckProperties.languageExtension;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (hcName == null ? 0 : System.hashCode(hcName));
        hashCode = (17 * hashCode) + (hcTags == null ? 0 : System.hashCode(hcTags));
        hashCode = (17 * hashCode) + (hcMbeanName == null ? 0 : System.hashCode(hcMbeanName));
        hashCode = (17 * hashCode) + (expression == null ? 0 : System.hashCode(expression));
        hashCode = (17 * hashCode) + (languageExtension == null ? 0 : System.hashCode(languageExtension));
        return hashCode;
    }
}

