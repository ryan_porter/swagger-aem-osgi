/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheJackrabbitOakPluginsMetr
 */
public class OASOrgApacheJackrabbitOakPluginsMetr {
    /**
     * Get providerType
     * @return providerType
     */
    public OASConfigNodePropertyDropDown providerType { get; set; }

    public static OASOrgApacheJackrabbitOakPluginsMetr getExample() {
        OASOrgApacheJackrabbitOakPluginsMetr orgApacheJackrabbitOakPluginsMetricStatisticsProviderFactoryProperties = new OASOrgApacheJackrabbitOakPluginsMetr();
          orgApacheJackrabbitOakPluginsMetricStatisticsProviderFactoryProperties.providerType = OASConfigNodePropertyDropDown.getExample();
        return orgApacheJackrabbitOakPluginsMetricStatisticsProviderFactoryProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheJackrabbitOakPluginsMetr) {           
            OASOrgApacheJackrabbitOakPluginsMetr orgApacheJackrabbitOakPluginsMetricStatisticsProviderFactoryProperties = (OASOrgApacheJackrabbitOakPluginsMetr) obj;
            return this.providerType == orgApacheJackrabbitOakPluginsMetricStatisticsProviderFactoryProperties.providerType;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (providerType == null ? 0 : System.hashCode(providerType));
        return hashCode;
    }
}

