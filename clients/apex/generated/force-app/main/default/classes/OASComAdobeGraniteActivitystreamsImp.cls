/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteActivitystreamsImp
 */
public class OASComAdobeGraniteActivitystreamsImp implements OAS.MappedProperties {
    /**
     * Get aggregateRelationships
     * @return aggregateRelationships
     */
    public OASConfigNodePropertyArray aggregateRelationships { get; set; }

    /**
     * Get aggregateDescendVirtual
     * @return aggregateDescendVirtual
     */
    public OASConfigNodePropertyBoolean aggregateDescendVirtual { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'aggregate.relationships' => 'aggregateRelationships',
        'aggregate.descend.virtual' => 'aggregateDescendVirtual'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeGraniteActivitystreamsImp getExample() {
        OASComAdobeGraniteActivitystreamsImp comAdobeGraniteActivitystreamsImplActivityManagerImplProperties = new OASComAdobeGraniteActivitystreamsImp();
          comAdobeGraniteActivitystreamsImplActivityManagerImplProperties.aggregateRelationships = OASConfigNodePropertyArray.getExample();
          comAdobeGraniteActivitystreamsImplActivityManagerImplProperties.aggregateDescendVirtual = OASConfigNodePropertyBoolean.getExample();
        return comAdobeGraniteActivitystreamsImplActivityManagerImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteActivitystreamsImp) {           
            OASComAdobeGraniteActivitystreamsImp comAdobeGraniteActivitystreamsImplActivityManagerImplProperties = (OASComAdobeGraniteActivitystreamsImp) obj;
            return this.aggregateRelationships == comAdobeGraniteActivitystreamsImplActivityManagerImplProperties.aggregateRelationships
                && this.aggregateDescendVirtual == comAdobeGraniteActivitystreamsImplActivityManagerImplProperties.aggregateDescendVirtual;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (aggregateRelationships == null ? 0 : System.hashCode(aggregateRelationships));
        hashCode = (17 * hashCode) + (aggregateDescendVirtual == null ? 0 : System.hashCode(aggregateDescendVirtual));
        return hashCode;
    }
}

