/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASConfigNodePropertyDropDown
 */
public class OASConfigNodePropertyDropDown implements OAS.MappedProperties {
    /**
     * property name
     * @return name
     */
    public String name { get; set; }

    /**
     * True if optional
     * @return optional
     */
    public Boolean optional { get; set; }

    /**
     * True if property is set
     * @return isSet
     */
    public Boolean isSet { get; set; }

    /**
     * Get r_type
     * @return r_type
     */
    public OASConfigNodePropertyDropDownType r_type { get; set; }

    /**
     * Property value
     * @return value
     */
    public Object value { get; set; }

    /**
     * Property description
     * @return description
     */
    public String description { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'is_set' => 'isSet',
        'type' => 'r_type'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASConfigNodePropertyDropDown getExample() {
        OASConfigNodePropertyDropDown configNodePropertyDropDown = new OASConfigNodePropertyDropDown();
          configNodePropertyDropDown.name = '';
          configNodePropertyDropDown.optional = true;
          configNodePropertyDropDown.isSet = true;
          configNodePropertyDropDown.r_type = OASConfigNodePropertyDropDownType.getExample();
          configNodePropertyDropDown.value = Object.getExample();
          configNodePropertyDropDown.description = '';
        return configNodePropertyDropDown;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASConfigNodePropertyDropDown) {           
            OASConfigNodePropertyDropDown configNodePropertyDropDown = (OASConfigNodePropertyDropDown) obj;
            return this.name == configNodePropertyDropDown.name
                && this.optional == configNodePropertyDropDown.optional
                && this.isSet == configNodePropertyDropDown.isSet
                && this.r_type == configNodePropertyDropDown.r_type
                && this.value == configNodePropertyDropDown.value
                && this.description == configNodePropertyDropDown.description;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (name == null ? 0 : System.hashCode(name));
        hashCode = (17 * hashCode) + (optional == null ? 0 : System.hashCode(optional));
        hashCode = (17 * hashCode) + (isSet == null ? 0 : System.hashCode(isSet));
        hashCode = (17 * hashCode) + (r_type == null ? 0 : System.hashCode(r_type));
        hashCode = (17 * hashCode) + (value == null ? 0 : System.hashCode(value));
        hashCode = (17 * hashCode) + (description == null ? 0 : System.hashCode(description));
        return hashCode;
    }
}

