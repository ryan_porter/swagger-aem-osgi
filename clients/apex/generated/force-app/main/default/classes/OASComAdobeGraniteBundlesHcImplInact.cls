/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteBundlesHcImplInact
 */
public class OASComAdobeGraniteBundlesHcImplInact implements OAS.MappedProperties {
    /**
     * Get hcTags
     * @return hcTags
     */
    public OASConfigNodePropertyArray hcTags { get; set; }

    /**
     * Get ignoredBundles
     * @return ignoredBundles
     */
    public OASConfigNodePropertyArray ignoredBundles { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'hc.tags' => 'hcTags',
        'ignored.bundles' => 'ignoredBundles'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeGraniteBundlesHcImplInact getExample() {
        OASComAdobeGraniteBundlesHcImplInact comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties = new OASComAdobeGraniteBundlesHcImplInact();
          comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties.hcTags = OASConfigNodePropertyArray.getExample();
          comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties.ignoredBundles = OASConfigNodePropertyArray.getExample();
        return comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteBundlesHcImplInact) {           
            OASComAdobeGraniteBundlesHcImplInact comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties = (OASComAdobeGraniteBundlesHcImplInact) obj;
            return this.hcTags == comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties.hcTags
                && this.ignoredBundles == comAdobeGraniteBundlesHcImplInactiveBundlesHealthCheckProperties.ignoredBundles;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (hcTags == null ? 0 : System.hashCode(hcTags));
        hashCode = (17 * hashCode) + (ignoredBundles == null ? 0 : System.hashCode(ignoredBundles));
        return hashCode;
    }
}

