@isTest
private class OASOrgApacheJackrabbitOakSegmentSegmTest {
    @isTest
    private static void equalsSameInstance() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2 = orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1;
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3 = new OASOrgApacheJackrabbitOakSegmentSegm();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4 = orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3;

        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3 = new OASOrgApacheJackrabbitOakSegmentSegm();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4 = new OASOrgApacheJackrabbitOakSegmentSegm();

        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2 = new OASOrgApacheJackrabbitOakSegmentSegm();

        System.assertEquals(false, orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.equals('foo'));
        System.assertEquals(false, orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2 = new OASOrgApacheJackrabbitOakSegmentSegm();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3;

        System.assertEquals(false, orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3));
        System.assertEquals(false, orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2 = new OASOrgApacheJackrabbitOakSegmentSegm();

        System.assertEquals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.hashCode(), orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.hashCode());
        System.assertEquals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.hashCode(), orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2 = OASOrgApacheJackrabbitOakSegmentSegm.getExample();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3 = new OASOrgApacheJackrabbitOakSegmentSegm();
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4 = new OASOrgApacheJackrabbitOakSegmentSegm();

        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2));
        System.assert(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3.equals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4));
        System.assertEquals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties1.hashCode(), orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties2.hashCode());
        System.assertEquals(orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties3.hashCode(), orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties4.hashCode());
    }

    @isTest
    private static void maintainRenamedProperties() {
        OASOrgApacheJackrabbitOakSegmentSegm orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties = new OASOrgApacheJackrabbitOakSegmentSegm();
        Map<String, String> propertyMappings = orgApacheJackrabbitOakSegmentSegmentNodeStoreFactoryProperties.getPropertyMappings();
        System.assertEquals('repositoryHome', propertyMappings.get('repository.home'));
        System.assertEquals('tarmkMode', propertyMappings.get('tarmk.mode'));
        System.assertEquals('tarmkSize', propertyMappings.get('tarmk.size'));
        System.assertEquals('segmentCacheSize', propertyMappings.get('segmentCache.size'));
        System.assertEquals('stringCacheSize', propertyMappings.get('stringCache.size'));
        System.assertEquals('templateCacheSize', propertyMappings.get('templateCache.size'));
        System.assertEquals('stringDeduplicationCacheSize', propertyMappings.get('stringDeduplicationCache.size'));
        System.assertEquals('templateDeduplicationCacheSize', propertyMappings.get('templateDeduplicationCache.size'));
        System.assertEquals('nodeDeduplicationCacheSize', propertyMappings.get('nodeDeduplicationCache.size'));
        System.assertEquals('compactionRetryCount', propertyMappings.get('compaction.retryCount'));
        System.assertEquals('compactionForceTimeout', propertyMappings.get('compaction.force.timeout'));
        System.assertEquals('compactionSizeDeltaEstimation', propertyMappings.get('compaction.sizeDeltaEstimation'));
        System.assertEquals('compactionDisableEstimation', propertyMappings.get('compaction.disableEstimation'));
        System.assertEquals('compactionRetainedGenerations', propertyMappings.get('compaction.retainedGenerations'));
        System.assertEquals('compactionMemoryThreshold', propertyMappings.get('compaction.memoryThreshold'));
        System.assertEquals('compactionProgressLog', propertyMappings.get('compaction.progressLog'));
        System.assertEquals('repositoryBackupDir', propertyMappings.get('repository.backup.dir'));
    }
}
