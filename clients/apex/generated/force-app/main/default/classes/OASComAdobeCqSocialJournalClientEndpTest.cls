@isTest
private class OASComAdobeCqSocialJournalClientEndpTest {
    @isTest
    private static void equalsSameInstance() {
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2 = comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1;
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3 = new OASComAdobeCqSocialJournalClientEndp();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4 = comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3;

        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3 = new OASComAdobeCqSocialJournalClientEndp();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4 = new OASComAdobeCqSocialJournalClientEndp();

        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2 = new OASComAdobeCqSocialJournalClientEndp();

        System.assertEquals(false, comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.equals('foo'));
        System.assertEquals(false, comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2 = new OASComAdobeCqSocialJournalClientEndp();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3;

        System.assertEquals(false, comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3));
        System.assertEquals(false, comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2 = new OASComAdobeCqSocialJournalClientEndp();

        System.assertEquals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.hashCode(), comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.hashCode());
        System.assertEquals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.hashCode(), comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2 = OASComAdobeCqSocialJournalClientEndp.getExample();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3 = new OASComAdobeCqSocialJournalClientEndp();
        OASComAdobeCqSocialJournalClientEndp comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4 = new OASComAdobeCqSocialJournalClientEndp();

        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2));
        System.assert(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3.equals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4));
        System.assertEquals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties1.hashCode(), comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties2.hashCode());
        System.assertEquals(comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties3.hashCode(), comAdobeCqSocialJournalClientEndpointsImplJournalOperationsSerProperties4.hashCode());
    }
}
