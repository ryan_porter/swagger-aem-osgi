/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWorkflowImplEmailEMailNot
 */
public class OASComDayCqWorkflowImplEmailEMailNot implements OAS.MappedProperties {
    /**
     * Get fromAddress
     * @return fromAddress
     */
    public OASConfigNodePropertyString fromAddress { get; set; }

    /**
     * Get hostPrefix
     * @return hostPrefix
     */
    public OASConfigNodePropertyString hostPrefix { get; set; }

    /**
     * Get notifyOnabort
     * @return notifyOnabort
     */
    public OASConfigNodePropertyBoolean notifyOnabort { get; set; }

    /**
     * Get notifyOncomplete
     * @return notifyOncomplete
     */
    public OASConfigNodePropertyBoolean notifyOncomplete { get; set; }

    /**
     * Get notifyOncontainercomplete
     * @return notifyOncontainercomplete
     */
    public OASConfigNodePropertyBoolean notifyOncontainercomplete { get; set; }

    /**
     * Get notifyUseronly
     * @return notifyUseronly
     */
    public OASConfigNodePropertyBoolean notifyUseronly { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'from.address' => 'fromAddress',
        'host.prefix' => 'hostPrefix',
        'notify.onabort' => 'notifyOnabort',
        'notify.oncomplete' => 'notifyOncomplete',
        'notify.oncontainercomplete' => 'notifyOncontainercomplete',
        'notify.useronly' => 'notifyUseronly'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqWorkflowImplEmailEMailNot getExample() {
        OASComDayCqWorkflowImplEmailEMailNot comDayCqWorkflowImplEmailEMailNotificationServiceProperties = new OASComDayCqWorkflowImplEmailEMailNot();
          comDayCqWorkflowImplEmailEMailNotificationServiceProperties.fromAddress = OASConfigNodePropertyString.getExample();
          comDayCqWorkflowImplEmailEMailNotificationServiceProperties.hostPrefix = OASConfigNodePropertyString.getExample();
          comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyOnabort = OASConfigNodePropertyBoolean.getExample();
          comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyOncomplete = OASConfigNodePropertyBoolean.getExample();
          comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyOncontainercomplete = OASConfigNodePropertyBoolean.getExample();
          comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyUseronly = OASConfigNodePropertyBoolean.getExample();
        return comDayCqWorkflowImplEmailEMailNotificationServiceProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWorkflowImplEmailEMailNot) {           
            OASComDayCqWorkflowImplEmailEMailNot comDayCqWorkflowImplEmailEMailNotificationServiceProperties = (OASComDayCqWorkflowImplEmailEMailNot) obj;
            return this.fromAddress == comDayCqWorkflowImplEmailEMailNotificationServiceProperties.fromAddress
                && this.hostPrefix == comDayCqWorkflowImplEmailEMailNotificationServiceProperties.hostPrefix
                && this.notifyOnabort == comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyOnabort
                && this.notifyOncomplete == comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyOncomplete
                && this.notifyOncontainercomplete == comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyOncontainercomplete
                && this.notifyUseronly == comDayCqWorkflowImplEmailEMailNotificationServiceProperties.notifyUseronly;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (fromAddress == null ? 0 : System.hashCode(fromAddress));
        hashCode = (17 * hashCode) + (hostPrefix == null ? 0 : System.hashCode(hostPrefix));
        hashCode = (17 * hashCode) + (notifyOnabort == null ? 0 : System.hashCode(notifyOnabort));
        hashCode = (17 * hashCode) + (notifyOncomplete == null ? 0 : System.hashCode(notifyOncomplete));
        hashCode = (17 * hashCode) + (notifyOncontainercomplete == null ? 0 : System.hashCode(notifyOncontainercomplete));
        hashCode = (17 * hashCode) + (notifyUseronly == null ? 0 : System.hashCode(notifyUseronly));
        return hashCode;
    }
}

