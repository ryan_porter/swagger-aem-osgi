/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteBundlesHcImplDavEx
 */
public class OASComAdobeGraniteBundlesHcImplDavEx implements OAS.MappedProperties {
    /**
     * Get hcTags
     * @return hcTags
     */
    public OASConfigNodePropertyArray hcTags { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'hc.tags' => 'hcTags'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeGraniteBundlesHcImplDavEx getExample() {
        OASComAdobeGraniteBundlesHcImplDavEx comAdobeGraniteBundlesHcImplDavExBundleHealthCheckProperties = new OASComAdobeGraniteBundlesHcImplDavEx();
          comAdobeGraniteBundlesHcImplDavExBundleHealthCheckProperties.hcTags = OASConfigNodePropertyArray.getExample();
        return comAdobeGraniteBundlesHcImplDavExBundleHealthCheckProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteBundlesHcImplDavEx) {           
            OASComAdobeGraniteBundlesHcImplDavEx comAdobeGraniteBundlesHcImplDavExBundleHealthCheckProperties = (OASComAdobeGraniteBundlesHcImplDavEx) obj;
            return this.hcTags == comAdobeGraniteBundlesHcImplDavExBundleHealthCheckProperties.hcTags;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (hcTags == null ? 0 : System.hashCode(hcTags));
        return hashCode;
    }
}

