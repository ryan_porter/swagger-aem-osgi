/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamInddImplHandlerIndesig
 */
public class OASComDayCqDamInddImplHandlerIndesig implements OAS.MappedProperties {
    /**
     * Get processLabel
     * @return processLabel
     */
    public OASConfigNodePropertyString processLabel { get; set; }

    /**
     * Get extractPages
     * @return extractPages
     */
    public OASConfigNodePropertyBoolean extractPages { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'process.label' => 'processLabel',
        'extract.pages' => 'extractPages'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamInddImplHandlerIndesig getExample() {
        OASComDayCqDamInddImplHandlerIndesig comDayCqDamInddImplHandlerIndesignXMPHandlerProperties = new OASComDayCqDamInddImplHandlerIndesig();
          comDayCqDamInddImplHandlerIndesignXMPHandlerProperties.processLabel = OASConfigNodePropertyString.getExample();
          comDayCqDamInddImplHandlerIndesignXMPHandlerProperties.extractPages = OASConfigNodePropertyBoolean.getExample();
        return comDayCqDamInddImplHandlerIndesignXMPHandlerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamInddImplHandlerIndesig) {           
            OASComDayCqDamInddImplHandlerIndesig comDayCqDamInddImplHandlerIndesignXMPHandlerProperties = (OASComDayCqDamInddImplHandlerIndesig) obj;
            return this.processLabel == comDayCqDamInddImplHandlerIndesignXMPHandlerProperties.processLabel
                && this.extractPages == comDayCqDamInddImplHandlerIndesignXMPHandlerProperties.extractPages;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (processLabel == null ? 0 : System.hashCode(processLabel));
        hashCode = (17 * hashCode) + (extractPages == null ? 0 : System.hashCode(extractPages));
        return hashCode;
    }
}

