/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamCoreProcessExtractMeta
 */
public class OASComDayCqDamCoreProcessExtractMeta implements OAS.MappedProperties {
    /**
     * Get processLabel
     * @return processLabel
     */
    public OASConfigNodePropertyString processLabel { get; set; }

    /**
     * Get cqDamEnableSha1
     * @return cqDamEnableSha1
     */
    public OASConfigNodePropertyBoolean cqDamEnableSha1 { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'process.label' => 'processLabel',
        'cq.dam.enable.sha1' => 'cqDamEnableSha1'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamCoreProcessExtractMeta getExample() {
        OASComDayCqDamCoreProcessExtractMeta comDayCqDamCoreProcessExtractMetadataProcessProperties = new OASComDayCqDamCoreProcessExtractMeta();
          comDayCqDamCoreProcessExtractMetadataProcessProperties.processLabel = OASConfigNodePropertyString.getExample();
          comDayCqDamCoreProcessExtractMetadataProcessProperties.cqDamEnableSha1 = OASConfigNodePropertyBoolean.getExample();
        return comDayCqDamCoreProcessExtractMetadataProcessProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamCoreProcessExtractMeta) {           
            OASComDayCqDamCoreProcessExtractMeta comDayCqDamCoreProcessExtractMetadataProcessProperties = (OASComDayCqDamCoreProcessExtractMeta) obj;
            return this.processLabel == comDayCqDamCoreProcessExtractMetadataProcessProperties.processLabel
                && this.cqDamEnableSha1 == comDayCqDamCoreProcessExtractMetadataProcessProperties.cqDamEnableSha1;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (processLabel == null ? 0 : System.hashCode(processLabel));
        hashCode = (17 * hashCode) + (cqDamEnableSha1 == null ? 0 : System.hashCode(cqDamEnableSha1));
        return hashCode;
    }
}

