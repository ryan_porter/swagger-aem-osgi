/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqScreensSegmentationImpl
 */
public class OASComAdobeCqScreensSegmentationImpl {
    /**
     * Get enableDataTriggeredContent
     * @return enableDataTriggeredContent
     */
    public OASConfigNodePropertyBoolean enableDataTriggeredContent { get; set; }

    public static OASComAdobeCqScreensSegmentationImpl getExample() {
        OASComAdobeCqScreensSegmentationImpl comAdobeCqScreensSegmentationImplSegmentationFeatureFlagProperties = new OASComAdobeCqScreensSegmentationImpl();
          comAdobeCqScreensSegmentationImplSegmentationFeatureFlagProperties.enableDataTriggeredContent = OASConfigNodePropertyBoolean.getExample();
        return comAdobeCqScreensSegmentationImplSegmentationFeatureFlagProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqScreensSegmentationImpl) {           
            OASComAdobeCqScreensSegmentationImpl comAdobeCqScreensSegmentationImplSegmentationFeatureFlagProperties = (OASComAdobeCqScreensSegmentationImpl) obj;
            return this.enableDataTriggeredContent == comAdobeCqScreensSegmentationImplSegmentationFeatureFlagProperties.enableDataTriggeredContent;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (enableDataTriggeredContent == null ? 0 : System.hashCode(enableDataTriggeredContent));
        return hashCode;
    }
}

