/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheSlingCaconfigImplConfigu
 */
public class OASOrgApacheSlingCaconfigImplConfigu {
    /**
     * Get configBucketNames
     * @return configBucketNames
     */
    public OASConfigNodePropertyArray configBucketNames { get; set; }

    public static OASOrgApacheSlingCaconfigImplConfigu getExample() {
        OASOrgApacheSlingCaconfigImplConfigu orgApacheSlingCaconfigImplConfigurationResolverImplProperties = new OASOrgApacheSlingCaconfigImplConfigu();
          orgApacheSlingCaconfigImplConfigurationResolverImplProperties.configBucketNames = OASConfigNodePropertyArray.getExample();
        return orgApacheSlingCaconfigImplConfigurationResolverImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheSlingCaconfigImplConfigu) {           
            OASOrgApacheSlingCaconfigImplConfigu orgApacheSlingCaconfigImplConfigurationResolverImplProperties = (OASOrgApacheSlingCaconfigImplConfigu) obj;
            return this.configBucketNames == orgApacheSlingCaconfigImplConfigurationResolverImplProperties.configBucketNames;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (configBucketNames == null ? 0 : System.hashCode(configBucketNames));
        return hashCode;
    }
}

