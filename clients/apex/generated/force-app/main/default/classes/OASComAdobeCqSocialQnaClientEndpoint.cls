/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqSocialQnaClientEndpoint
 */
public class OASComAdobeCqSocialQnaClientEndpoint {
    /**
     * Get fieldWhitelist
     * @return fieldWhitelist
     */
    public OASConfigNodePropertyArray fieldWhitelist { get; set; }

    /**
     * Get attachmentTypeBlacklist
     * @return attachmentTypeBlacklist
     */
    public OASConfigNodePropertyArray attachmentTypeBlacklist { get; set; }

    public static OASComAdobeCqSocialQnaClientEndpoint getExample() {
        OASComAdobeCqSocialQnaClientEndpoint comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties = new OASComAdobeCqSocialQnaClientEndpoint();
          comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties.fieldWhitelist = OASConfigNodePropertyArray.getExample();
          comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties.attachmentTypeBlacklist = OASConfigNodePropertyArray.getExample();
        return comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqSocialQnaClientEndpoint) {           
            OASComAdobeCqSocialQnaClientEndpoint comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties = (OASComAdobeCqSocialQnaClientEndpoint) obj;
            return this.fieldWhitelist == comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties.fieldWhitelist
                && this.attachmentTypeBlacklist == comAdobeCqSocialQnaClientEndpointsImplQnaForumOperationsServicProperties.attachmentTypeBlacklist;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (fieldWhitelist == null ? 0 : System.hashCode(fieldWhitelist));
        hashCode = (17 * hashCode) + (attachmentTypeBlacklist == null ? 0 : System.hashCode(attachmentTypeBlacklist));
        return hashCode;
    }
}

