/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamS7damCommonS7damDamCha
 */
public class OASComDayCqDamS7damCommonS7damDamCha implements OAS.MappedProperties {
    /**
     * Get cqDamS7damDamchangeeventlistenerEnabled
     * @return cqDamS7damDamchangeeventlistenerEnabled
     */
    public OASConfigNodePropertyBoolean cqDamS7damDamchangeeventlistenerEnabled { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'cq.dam.s7dam.damchangeeventlistener.enabled' => 'cqDamS7damDamchangeeventlistenerEnabled'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamS7damCommonS7damDamCha getExample() {
        OASComDayCqDamS7damCommonS7damDamCha comDayCqDamS7damCommonS7damDamChangeEventListenerProperties = new OASComDayCqDamS7damCommonS7damDamCha();
          comDayCqDamS7damCommonS7damDamChangeEventListenerProperties.cqDamS7damDamchangeeventlistenerEnabled = OASConfigNodePropertyBoolean.getExample();
        return comDayCqDamS7damCommonS7damDamChangeEventListenerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamS7damCommonS7damDamCha) {           
            OASComDayCqDamS7damCommonS7damDamCha comDayCqDamS7damCommonS7damDamChangeEventListenerProperties = (OASComDayCqDamS7damCommonS7damDamCha) obj;
            return this.cqDamS7damDamchangeeventlistenerEnabled == comDayCqDamS7damCommonS7damDamChangeEventListenerProperties.cqDamS7damDamchangeeventlistenerEnabled;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (cqDamS7damDamchangeeventlistenerEnabled == null ? 0 : System.hashCode(cqDamS7damDamchangeeventlistenerEnabled));
        return hashCode;
    }
}

