@isTest
private class OASComAdobeCqSocialCalendarClientOpeTest {
    @isTest
    private static void equalsSameInstance() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2 = comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1;
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3 = new OASComAdobeCqSocialCalendarClientOpe();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4 = comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3;

        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3 = new OASComAdobeCqSocialCalendarClientOpe();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4 = new OASComAdobeCqSocialCalendarClientOpe();

        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2 = new OASComAdobeCqSocialCalendarClientOpe();

        System.assertEquals(false, comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.equals('foo'));
        System.assertEquals(false, comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2 = new OASComAdobeCqSocialCalendarClientOpe();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3;

        System.assertEquals(false, comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3));
        System.assertEquals(false, comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2 = new OASComAdobeCqSocialCalendarClientOpe();

        System.assertEquals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.hashCode(), comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.hashCode());
        System.assertEquals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.hashCode(), comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2 = OASComAdobeCqSocialCalendarClientOpe.getExample();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3 = new OASComAdobeCqSocialCalendarClientOpe();
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4 = new OASComAdobeCqSocialCalendarClientOpe();

        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2));
        System.assert(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3.equals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4));
        System.assertEquals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties1.hashCode(), comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties2.hashCode());
        System.assertEquals(comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties3.hashCode(), comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties4.hashCode());
    }

    @isTest
    private static void maintainRenamedProperties() {
        OASComAdobeCqSocialCalendarClientOpe comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties = new OASComAdobeCqSocialCalendarClientOpe();
        Map<String, String> propertyMappings = comAdobeCqSocialCalendarClientOperationextensionsEventAttachmenProperties.getPropertyMappings();
        System.assertEquals('extensionOrder', propertyMappings.get('extension.order'));
    }
}
