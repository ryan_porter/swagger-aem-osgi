/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamIdsImplIDSJobProcessor
 */
public class OASComDayCqDamIdsImplIDSJobProcessor implements OAS.MappedProperties {
    /**
     * Get enableMultisession
     * @return enableMultisession
     */
    public OASConfigNodePropertyBoolean enableMultisession { get; set; }

    /**
     * Get idsCcEnable
     * @return idsCcEnable
     */
    public OASConfigNodePropertyBoolean idsCcEnable { get; set; }

    /**
     * Get enableRetry
     * @return enableRetry
     */
    public OASConfigNodePropertyBoolean enableRetry { get; set; }

    /**
     * Get enableRetryScripterror
     * @return enableRetryScripterror
     */
    public OASConfigNodePropertyBoolean enableRetryScripterror { get; set; }

    /**
     * Get externalizerDomainCqhost
     * @return externalizerDomainCqhost
     */
    public OASConfigNodePropertyString externalizerDomainCqhost { get; set; }

    /**
     * Get externalizerDomainHttp
     * @return externalizerDomainHttp
     */
    public OASConfigNodePropertyString externalizerDomainHttp { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'enable.multisession' => 'enableMultisession',
        'ids.cc.enable' => 'idsCcEnable',
        'enable.retry' => 'enableRetry',
        'enable.retry.scripterror' => 'enableRetryScripterror',
        'externalizer.domain.cqhost' => 'externalizerDomainCqhost',
        'externalizer.domain.http' => 'externalizerDomainHttp'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamIdsImplIDSJobProcessor getExample() {
        OASComDayCqDamIdsImplIDSJobProcessor comDayCqDamIdsImplIDSJobProcessorProperties = new OASComDayCqDamIdsImplIDSJobProcessor();
          comDayCqDamIdsImplIDSJobProcessorProperties.enableMultisession = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamIdsImplIDSJobProcessorProperties.idsCcEnable = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamIdsImplIDSJobProcessorProperties.enableRetry = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamIdsImplIDSJobProcessorProperties.enableRetryScripterror = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamIdsImplIDSJobProcessorProperties.externalizerDomainCqhost = OASConfigNodePropertyString.getExample();
          comDayCqDamIdsImplIDSJobProcessorProperties.externalizerDomainHttp = OASConfigNodePropertyString.getExample();
        return comDayCqDamIdsImplIDSJobProcessorProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamIdsImplIDSJobProcessor) {           
            OASComDayCqDamIdsImplIDSJobProcessor comDayCqDamIdsImplIDSJobProcessorProperties = (OASComDayCqDamIdsImplIDSJobProcessor) obj;
            return this.enableMultisession == comDayCqDamIdsImplIDSJobProcessorProperties.enableMultisession
                && this.idsCcEnable == comDayCqDamIdsImplIDSJobProcessorProperties.idsCcEnable
                && this.enableRetry == comDayCqDamIdsImplIDSJobProcessorProperties.enableRetry
                && this.enableRetryScripterror == comDayCqDamIdsImplIDSJobProcessorProperties.enableRetryScripterror
                && this.externalizerDomainCqhost == comDayCqDamIdsImplIDSJobProcessorProperties.externalizerDomainCqhost
                && this.externalizerDomainHttp == comDayCqDamIdsImplIDSJobProcessorProperties.externalizerDomainHttp;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (enableMultisession == null ? 0 : System.hashCode(enableMultisession));
        hashCode = (17 * hashCode) + (idsCcEnable == null ? 0 : System.hashCode(idsCcEnable));
        hashCode = (17 * hashCode) + (enableRetry == null ? 0 : System.hashCode(enableRetry));
        hashCode = (17 * hashCode) + (enableRetryScripterror == null ? 0 : System.hashCode(enableRetryScripterror));
        hashCode = (17 * hashCode) + (externalizerDomainCqhost == null ? 0 : System.hashCode(externalizerDomainCqhost));
        hashCode = (17 * hashCode) + (externalizerDomainHttp == null ? 0 : System.hashCode(externalizerDomainHttp));
        return hashCode;
    }
}

