/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheJackrabbitOakPluginsBlob
 */
public class OASOrgApacheJackrabbitOakPluginsBlob {
    /**
     * Get path
     * @return path
     */
    public OASConfigNodePropertyString path { get; set; }

    public static OASOrgApacheJackrabbitOakPluginsBlob getExample() {
        OASOrgApacheJackrabbitOakPluginsBlob orgApacheJackrabbitOakPluginsBlobDatastoreFileDataStoreProperties = new OASOrgApacheJackrabbitOakPluginsBlob();
          orgApacheJackrabbitOakPluginsBlobDatastoreFileDataStoreProperties.path = OASConfigNodePropertyString.getExample();
        return orgApacheJackrabbitOakPluginsBlobDatastoreFileDataStoreProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheJackrabbitOakPluginsBlob) {           
            OASOrgApacheJackrabbitOakPluginsBlob orgApacheJackrabbitOakPluginsBlobDatastoreFileDataStoreProperties = (OASOrgApacheJackrabbitOakPluginsBlob) obj;
            return this.path == orgApacheJackrabbitOakPluginsBlobDatastoreFileDataStoreProperties.path;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (path == null ? 0 : System.hashCode(path));
        return hashCode;
    }
}

