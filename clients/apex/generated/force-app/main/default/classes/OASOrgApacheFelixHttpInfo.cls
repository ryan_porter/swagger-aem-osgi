/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheFelixHttpInfo
 */
public class OASOrgApacheFelixHttpInfo implements OAS.MappedProperties {
    /**
     * Get pid
     * @return pid
     */
    public String pid { get; set; }

    /**
     * Get title
     * @return title
     */
    public String title { get; set; }

    /**
     * Get description
     * @return description
     */
    public String description { get; set; }

    /**
     * Get properties
     * @return properties
     */
    public OASOrgApacheFelixHttpProperties properties { get; set; }

    /**
     * Get bundleLocation
     * @return bundleLocation
     */
    public String bundleLocation { get; set; }

    /**
     * Get serviceLocation
     * @return serviceLocation
     */
    public String serviceLocation { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'bundle_location' => 'bundleLocation',
        'service_location' => 'serviceLocation'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheFelixHttpInfo getExample() {
        OASOrgApacheFelixHttpInfo orgApacheFelixHttpInfo = new OASOrgApacheFelixHttpInfo();
          orgApacheFelixHttpInfo.pid = '';
          orgApacheFelixHttpInfo.title = '';
          orgApacheFelixHttpInfo.description = '';
          orgApacheFelixHttpInfo.properties = OASOrgApacheFelixHttpProperties.getExample();
          orgApacheFelixHttpInfo.bundleLocation = '';
          orgApacheFelixHttpInfo.serviceLocation = '';
        return orgApacheFelixHttpInfo;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheFelixHttpInfo) {           
            OASOrgApacheFelixHttpInfo orgApacheFelixHttpInfo = (OASOrgApacheFelixHttpInfo) obj;
            return this.pid == orgApacheFelixHttpInfo.pid
                && this.title == orgApacheFelixHttpInfo.title
                && this.description == orgApacheFelixHttpInfo.description
                && this.properties == orgApacheFelixHttpInfo.properties
                && this.bundleLocation == orgApacheFelixHttpInfo.bundleLocation
                && this.serviceLocation == orgApacheFelixHttpInfo.serviceLocation;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (pid == null ? 0 : System.hashCode(pid));
        hashCode = (17 * hashCode) + (title == null ? 0 : System.hashCode(title));
        hashCode = (17 * hashCode) + (description == null ? 0 : System.hashCode(description));
        hashCode = (17 * hashCode) + (properties == null ? 0 : System.hashCode(properties));
        hashCode = (17 * hashCode) + (bundleLocation == null ? 0 : System.hashCode(bundleLocation));
        hashCode = (17 * hashCode) + (serviceLocation == null ? 0 : System.hashCode(serviceLocation));
        return hashCode;
    }
}

