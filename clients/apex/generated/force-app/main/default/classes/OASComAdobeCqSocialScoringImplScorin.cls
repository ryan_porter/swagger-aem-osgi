/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqSocialScoringImplScorin
 */
public class OASComAdobeCqSocialScoringImplScorin implements OAS.MappedProperties {
    /**
     * Get eventTopics
     * @return eventTopics
     */
    public OASConfigNodePropertyString eventTopics { get; set; }

    /**
     * Get eventFilter
     * @return eventFilter
     */
    public OASConfigNodePropertyString eventFilter { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'event.topics' => 'eventTopics',
        'event.filter' => 'eventFilter'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqSocialScoringImplScorin getExample() {
        OASComAdobeCqSocialScoringImplScorin comAdobeCqSocialScoringImplScoringEventListenerProperties = new OASComAdobeCqSocialScoringImplScorin();
          comAdobeCqSocialScoringImplScoringEventListenerProperties.eventTopics = OASConfigNodePropertyString.getExample();
          comAdobeCqSocialScoringImplScoringEventListenerProperties.eventFilter = OASConfigNodePropertyString.getExample();
        return comAdobeCqSocialScoringImplScoringEventListenerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqSocialScoringImplScorin) {           
            OASComAdobeCqSocialScoringImplScorin comAdobeCqSocialScoringImplScoringEventListenerProperties = (OASComAdobeCqSocialScoringImplScorin) obj;
            return this.eventTopics == comAdobeCqSocialScoringImplScoringEventListenerProperties.eventTopics
                && this.eventFilter == comAdobeCqSocialScoringImplScoringEventListenerProperties.eventFilter;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (eventTopics == null ? 0 : System.hashCode(eventTopics));
        hashCode = (17 * hashCode) + (eventFilter == null ? 0 : System.hashCode(eventFilter));
        return hashCode;
    }
}

