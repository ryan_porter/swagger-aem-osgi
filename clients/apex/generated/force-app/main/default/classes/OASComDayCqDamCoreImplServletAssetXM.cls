/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamCoreImplServletAssetXM
 */
public class OASComDayCqDamCoreImplServletAssetXM implements OAS.MappedProperties {
    /**
     * Get cqDamBatchIndesignMaxassets
     * @return cqDamBatchIndesignMaxassets
     */
    public OASConfigNodePropertyInteger cqDamBatchIndesignMaxassets { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'cq.dam.batch.indesign.maxassets' => 'cqDamBatchIndesignMaxassets'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamCoreImplServletAssetXM getExample() {
        OASComDayCqDamCoreImplServletAssetXM comDayCqDamCoreImplServletAssetXMPSearchServletProperties = new OASComDayCqDamCoreImplServletAssetXM();
          comDayCqDamCoreImplServletAssetXMPSearchServletProperties.cqDamBatchIndesignMaxassets = OASConfigNodePropertyInteger.getExample();
        return comDayCqDamCoreImplServletAssetXMPSearchServletProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamCoreImplServletAssetXM) {           
            OASComDayCqDamCoreImplServletAssetXM comDayCqDamCoreImplServletAssetXMPSearchServletProperties = (OASComDayCqDamCoreImplServletAssetXM) obj;
            return this.cqDamBatchIndesignMaxassets == comDayCqDamCoreImplServletAssetXMPSearchServletProperties.cqDamBatchIndesignMaxassets;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (cqDamBatchIndesignMaxassets == null ? 0 : System.hashCode(cqDamBatchIndesignMaxassets));
        return hashCode;
    }
}

