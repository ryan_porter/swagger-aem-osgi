/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamScene7ImplScene7DamCha
 */
public class OASComDayCqDamScene7ImplScene7DamCha implements OAS.MappedProperties {
    /**
     * Get cqDamScene7DamchangeeventlistenerEnabled
     * @return cqDamScene7DamchangeeventlistenerEnabled
     */
    public OASConfigNodePropertyBoolean cqDamScene7DamchangeeventlistenerEnabled { get; set; }

    /**
     * Get cqDamScene7DamchangeeventlistenerObservedPaths
     * @return cqDamScene7DamchangeeventlistenerObservedPaths
     */
    public OASConfigNodePropertyArray cqDamScene7DamchangeeventlistenerObservedPaths { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'cq.dam.scene7.damchangeeventlistener.enabled' => 'cqDamScene7DamchangeeventlistenerEnabled',
        'cq.dam.scene7.damchangeeventlistener.observed.paths' => 'cqDamScene7DamchangeeventlistenerObservedPaths'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamScene7ImplScene7DamCha getExample() {
        OASComDayCqDamScene7ImplScene7DamCha comDayCqDamScene7ImplScene7DamChangeEventListenerProperties = new OASComDayCqDamScene7ImplScene7DamCha();
          comDayCqDamScene7ImplScene7DamChangeEventListenerProperties.cqDamScene7DamchangeeventlistenerEnabled = OASConfigNodePropertyBoolean.getExample();
          comDayCqDamScene7ImplScene7DamChangeEventListenerProperties.cqDamScene7DamchangeeventlistenerObservedPaths = OASConfigNodePropertyArray.getExample();
        return comDayCqDamScene7ImplScene7DamChangeEventListenerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamScene7ImplScene7DamCha) {           
            OASComDayCqDamScene7ImplScene7DamCha comDayCqDamScene7ImplScene7DamChangeEventListenerProperties = (OASComDayCqDamScene7ImplScene7DamCha) obj;
            return this.cqDamScene7DamchangeeventlistenerEnabled == comDayCqDamScene7ImplScene7DamChangeEventListenerProperties.cqDamScene7DamchangeeventlistenerEnabled
                && this.cqDamScene7DamchangeeventlistenerObservedPaths == comDayCqDamScene7ImplScene7DamChangeEventListenerProperties.cqDamScene7DamchangeeventlistenerObservedPaths;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (cqDamScene7DamchangeeventlistenerEnabled == null ? 0 : System.hashCode(cqDamScene7DamchangeeventlistenerEnabled));
        hashCode = (17 * hashCode) + (cqDamScene7DamchangeeventlistenerObservedPaths == null ? 0 : System.hashCode(cqDamScene7DamchangeeventlistenerObservedPaths));
        return hashCode;
    }
}

