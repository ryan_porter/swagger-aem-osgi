/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqNotificationImplNotificat
 */
public class OASComDayCqNotificationImplNotificat implements OAS.MappedProperties {
    /**
     * Get eventFilter
     * @return eventFilter
     */
    public OASConfigNodePropertyString eventFilter { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'event.filter' => 'eventFilter'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqNotificationImplNotificat getExample() {
        OASComDayCqNotificationImplNotificat comDayCqNotificationImplNotificationServiceImplProperties = new OASComDayCqNotificationImplNotificat();
          comDayCqNotificationImplNotificationServiceImplProperties.eventFilter = OASConfigNodePropertyString.getExample();
        return comDayCqNotificationImplNotificationServiceImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqNotificationImplNotificat) {           
            OASComDayCqNotificationImplNotificat comDayCqNotificationImplNotificationServiceImplProperties = (OASComDayCqNotificationImplNotificat) obj;
            return this.eventFilter == comDayCqNotificationImplNotificationServiceImplProperties.eventFilter;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (eventFilter == null ? 0 : System.hashCode(eventFilter));
        return hashCode;
    }
}

