/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWcmMobileCoreImplDeviceDe
 */
public class OASComDayCqWcmMobileCoreImplDeviceDe implements OAS.MappedProperties {
    /**
     * Get deviceInfoTransformerEnabled
     * @return deviceInfoTransformerEnabled
     */
    public OASConfigNodePropertyBoolean deviceInfoTransformerEnabled { get; set; }

    /**
     * Get deviceInfoTransformerCssStyle
     * @return deviceInfoTransformerCssStyle
     */
    public OASConfigNodePropertyString deviceInfoTransformerCssStyle { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'device.info.transformer.enabled' => 'deviceInfoTransformerEnabled',
        'device.info.transformer.css.style' => 'deviceInfoTransformerCssStyle'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqWcmMobileCoreImplDeviceDe getExample() {
        OASComDayCqWcmMobileCoreImplDeviceDe comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties = new OASComDayCqWcmMobileCoreImplDeviceDe();
          comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties.deviceInfoTransformerEnabled = OASConfigNodePropertyBoolean.getExample();
          comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties.deviceInfoTransformerCssStyle = OASConfigNodePropertyString.getExample();
        return comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWcmMobileCoreImplDeviceDe) {           
            OASComDayCqWcmMobileCoreImplDeviceDe comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties = (OASComDayCqWcmMobileCoreImplDeviceDe) obj;
            return this.deviceInfoTransformerEnabled == comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties.deviceInfoTransformerEnabled
                && this.deviceInfoTransformerCssStyle == comDayCqWcmMobileCoreImplDeviceDeviceInfoTransformerFactoryProperties.deviceInfoTransformerCssStyle;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (deviceInfoTransformerEnabled == null ? 0 : System.hashCode(deviceInfoTransformerEnabled));
        hashCode = (17 * hashCode) + (deviceInfoTransformerCssStyle == null ? 0 : System.hashCode(deviceInfoTransformerCssStyle));
        return hashCode;
    }
}

