/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCrxSecurityTokenImplImplTok
 */
public class OASComDayCrxSecurityTokenImplImplTok implements OAS.MappedProperties {
    /**
     * Get path
     * @return path
     */
    public OASConfigNodePropertyString path { get; set; }

    /**
     * Get tokenRequiredAttr
     * @return tokenRequiredAttr
     */
    public OASConfigNodePropertyDropDown tokenRequiredAttr { get; set; }

    /**
     * Get tokenAlternateUrl
     * @return tokenAlternateUrl
     */
    public OASConfigNodePropertyString tokenAlternateUrl { get; set; }

    /**
     * Get tokenEncapsulated
     * @return tokenEncapsulated
     */
    public OASConfigNodePropertyBoolean tokenEncapsulated { get; set; }

    /**
     * Get skipTokenRefresh
     * @return skipTokenRefresh
     */
    public OASConfigNodePropertyArray skipTokenRefresh { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'token.required.attr' => 'tokenRequiredAttr',
        'token.alternate.url' => 'tokenAlternateUrl',
        'token.encapsulated' => 'tokenEncapsulated',
        'skip.token.refresh' => 'skipTokenRefresh'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCrxSecurityTokenImplImplTok getExample() {
        OASComDayCrxSecurityTokenImplImplTok comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties = new OASComDayCrxSecurityTokenImplImplTok();
          comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.path = OASConfigNodePropertyString.getExample();
          comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.tokenRequiredAttr = OASConfigNodePropertyDropDown.getExample();
          comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.tokenAlternateUrl = OASConfigNodePropertyString.getExample();
          comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.tokenEncapsulated = OASConfigNodePropertyBoolean.getExample();
          comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.skipTokenRefresh = OASConfigNodePropertyArray.getExample();
        return comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCrxSecurityTokenImplImplTok) {           
            OASComDayCrxSecurityTokenImplImplTok comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties = (OASComDayCrxSecurityTokenImplImplTok) obj;
            return this.path == comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.path
                && this.tokenRequiredAttr == comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.tokenRequiredAttr
                && this.tokenAlternateUrl == comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.tokenAlternateUrl
                && this.tokenEncapsulated == comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.tokenEncapsulated
                && this.skipTokenRefresh == comDayCrxSecurityTokenImplImplTokenAuthenticationHandlerProperties.skipTokenRefresh;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (path == null ? 0 : System.hashCode(path));
        hashCode = (17 * hashCode) + (tokenRequiredAttr == null ? 0 : System.hashCode(tokenRequiredAttr));
        hashCode = (17 * hashCode) + (tokenAlternateUrl == null ? 0 : System.hashCode(tokenAlternateUrl));
        hashCode = (17 * hashCode) + (tokenEncapsulated == null ? 0 : System.hashCode(tokenEncapsulated));
        hashCode = (17 * hashCode) + (skipTokenRefresh == null ? 0 : System.hashCode(skipTokenRefresh));
        return hashCode;
    }
}

