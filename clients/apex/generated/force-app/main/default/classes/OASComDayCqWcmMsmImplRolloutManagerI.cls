/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWcmMsmImplRolloutManagerI
 */
public class OASComDayCqWcmMsmImplRolloutManagerI implements OAS.MappedProperties {
    /**
     * Get eventFilter
     * @return eventFilter
     */
    public OASConfigNodePropertyString eventFilter { get; set; }

    /**
     * Get rolloutmgrExcludedpropsDefault
     * @return rolloutmgrExcludedpropsDefault
     */
    public OASConfigNodePropertyArray rolloutmgrExcludedpropsDefault { get; set; }

    /**
     * Get rolloutmgrExcludedparagraphpropsDefault
     * @return rolloutmgrExcludedparagraphpropsDefault
     */
    public OASConfigNodePropertyArray rolloutmgrExcludedparagraphpropsDefault { get; set; }

    /**
     * Get rolloutmgrExcludednodetypesDefault
     * @return rolloutmgrExcludednodetypesDefault
     */
    public OASConfigNodePropertyArray rolloutmgrExcludednodetypesDefault { get; set; }

    /**
     * Get rolloutmgrThreadpoolMaxsize
     * @return rolloutmgrThreadpoolMaxsize
     */
    public OASConfigNodePropertyInteger rolloutmgrThreadpoolMaxsize { get; set; }

    /**
     * Get rolloutmgrThreadpoolMaxshutdowntime
     * @return rolloutmgrThreadpoolMaxshutdowntime
     */
    public OASConfigNodePropertyInteger rolloutmgrThreadpoolMaxshutdowntime { get; set; }

    /**
     * Get rolloutmgrThreadpoolPriority
     * @return rolloutmgrThreadpoolPriority
     */
    public OASConfigNodePropertyDropDown rolloutmgrThreadpoolPriority { get; set; }

    /**
     * Get rolloutmgrCommitSize
     * @return rolloutmgrCommitSize
     */
    public OASConfigNodePropertyInteger rolloutmgrCommitSize { get; set; }

    /**
     * Get rolloutmgrConflicthandlingEnabled
     * @return rolloutmgrConflicthandlingEnabled
     */
    public OASConfigNodePropertyBoolean rolloutmgrConflicthandlingEnabled { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'event.filter' => 'eventFilter',
        'rolloutmgr.excludedprops.default' => 'rolloutmgrExcludedpropsDefault',
        'rolloutmgr.excludedparagraphprops.default' => 'rolloutmgrExcludedparagraphpropsDefault',
        'rolloutmgr.excludednodetypes.default' => 'rolloutmgrExcludednodetypesDefault',
        'rolloutmgr.threadpool.maxsize' => 'rolloutmgrThreadpoolMaxsize',
        'rolloutmgr.threadpool.maxshutdowntime' => 'rolloutmgrThreadpoolMaxshutdowntime',
        'rolloutmgr.threadpool.priority' => 'rolloutmgrThreadpoolPriority',
        'rolloutmgr.commit.size' => 'rolloutmgrCommitSize',
        'rolloutmgr.conflicthandling.enabled' => 'rolloutmgrConflicthandlingEnabled'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqWcmMsmImplRolloutManagerI getExample() {
        OASComDayCqWcmMsmImplRolloutManagerI comDayCqWcmMsmImplRolloutManagerImplProperties = new OASComDayCqWcmMsmImplRolloutManagerI();
          comDayCqWcmMsmImplRolloutManagerImplProperties.eventFilter = OASConfigNodePropertyString.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrExcludedpropsDefault = OASConfigNodePropertyArray.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrExcludedparagraphpropsDefault = OASConfigNodePropertyArray.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrExcludednodetypesDefault = OASConfigNodePropertyArray.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrThreadpoolMaxsize = OASConfigNodePropertyInteger.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrThreadpoolMaxshutdowntime = OASConfigNodePropertyInteger.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrThreadpoolPriority = OASConfigNodePropertyDropDown.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrCommitSize = OASConfigNodePropertyInteger.getExample();
          comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrConflicthandlingEnabled = OASConfigNodePropertyBoolean.getExample();
        return comDayCqWcmMsmImplRolloutManagerImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWcmMsmImplRolloutManagerI) {           
            OASComDayCqWcmMsmImplRolloutManagerI comDayCqWcmMsmImplRolloutManagerImplProperties = (OASComDayCqWcmMsmImplRolloutManagerI) obj;
            return this.eventFilter == comDayCqWcmMsmImplRolloutManagerImplProperties.eventFilter
                && this.rolloutmgrExcludedpropsDefault == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrExcludedpropsDefault
                && this.rolloutmgrExcludedparagraphpropsDefault == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrExcludedparagraphpropsDefault
                && this.rolloutmgrExcludednodetypesDefault == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrExcludednodetypesDefault
                && this.rolloutmgrThreadpoolMaxsize == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrThreadpoolMaxsize
                && this.rolloutmgrThreadpoolMaxshutdowntime == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrThreadpoolMaxshutdowntime
                && this.rolloutmgrThreadpoolPriority == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrThreadpoolPriority
                && this.rolloutmgrCommitSize == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrCommitSize
                && this.rolloutmgrConflicthandlingEnabled == comDayCqWcmMsmImplRolloutManagerImplProperties.rolloutmgrConflicthandlingEnabled;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (eventFilter == null ? 0 : System.hashCode(eventFilter));
        hashCode = (17 * hashCode) + (rolloutmgrExcludedpropsDefault == null ? 0 : System.hashCode(rolloutmgrExcludedpropsDefault));
        hashCode = (17 * hashCode) + (rolloutmgrExcludedparagraphpropsDefault == null ? 0 : System.hashCode(rolloutmgrExcludedparagraphpropsDefault));
        hashCode = (17 * hashCode) + (rolloutmgrExcludednodetypesDefault == null ? 0 : System.hashCode(rolloutmgrExcludednodetypesDefault));
        hashCode = (17 * hashCode) + (rolloutmgrThreadpoolMaxsize == null ? 0 : System.hashCode(rolloutmgrThreadpoolMaxsize));
        hashCode = (17 * hashCode) + (rolloutmgrThreadpoolMaxshutdowntime == null ? 0 : System.hashCode(rolloutmgrThreadpoolMaxshutdowntime));
        hashCode = (17 * hashCode) + (rolloutmgrThreadpoolPriority == null ? 0 : System.hashCode(rolloutmgrThreadpoolPriority));
        hashCode = (17 * hashCode) + (rolloutmgrCommitSize == null ? 0 : System.hashCode(rolloutmgrCommitSize));
        hashCode = (17 * hashCode) + (rolloutmgrConflicthandlingEnabled == null ? 0 : System.hashCode(rolloutmgrConflicthandlingEnabled));
        return hashCode;
    }
}

