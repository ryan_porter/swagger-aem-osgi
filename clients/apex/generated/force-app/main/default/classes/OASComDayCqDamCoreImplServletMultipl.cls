/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamCoreImplServletMultipl
 */
public class OASComDayCqDamCoreImplServletMultipl implements OAS.MappedProperties {
    /**
     * Get cqDamDrmEnable
     * @return cqDamDrmEnable
     */
    public OASConfigNodePropertyBoolean cqDamDrmEnable { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'cq.dam.drm.enable' => 'cqDamDrmEnable'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamCoreImplServletMultipl getExample() {
        OASComDayCqDamCoreImplServletMultipl comDayCqDamCoreImplServletMultipleLicenseAcceptServletProperties = new OASComDayCqDamCoreImplServletMultipl();
          comDayCqDamCoreImplServletMultipleLicenseAcceptServletProperties.cqDamDrmEnable = OASConfigNodePropertyBoolean.getExample();
        return comDayCqDamCoreImplServletMultipleLicenseAcceptServletProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamCoreImplServletMultipl) {           
            OASComDayCqDamCoreImplServletMultipl comDayCqDamCoreImplServletMultipleLicenseAcceptServletProperties = (OASComDayCqDamCoreImplServletMultipl) obj;
            return this.cqDamDrmEnable == comDayCqDamCoreImplServletMultipleLicenseAcceptServletProperties.cqDamDrmEnable;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (cqDamDrmEnable == null ? 0 : System.hashCode(cqDamDrmEnable));
        return hashCode;
    }
}

