/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWcmDesignimporterImplMobi
 */
public class OASComDayCqWcmDesignimporterImplMobi implements OAS.MappedProperties {
    /**
     * Get filepattern
     * @return filepattern
     */
    public OASConfigNodePropertyString filepattern { get; set; }

    /**
     * Get deviceGroups
     * @return deviceGroups
     */
    public OASConfigNodePropertyArray deviceGroups { get; set; }

    /**
     * Get buildPageNodes
     * @return buildPageNodes
     */
    public OASConfigNodePropertyBoolean buildPageNodes { get; set; }

    /**
     * Get buildClientLibs
     * @return buildClientLibs
     */
    public OASConfigNodePropertyBoolean buildClientLibs { get; set; }

    /**
     * Get buildCanvasComponent
     * @return buildCanvasComponent
     */
    public OASConfigNodePropertyBoolean buildCanvasComponent { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'device.groups' => 'deviceGroups',
        'build.page.nodes' => 'buildPageNodes',
        'build.client.libs' => 'buildClientLibs',
        'build.canvas.component' => 'buildCanvasComponent'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqWcmDesignimporterImplMobi getExample() {
        OASComDayCqWcmDesignimporterImplMobi comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties = new OASComDayCqWcmDesignimporterImplMobi();
          comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.filepattern = OASConfigNodePropertyString.getExample();
          comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.deviceGroups = OASConfigNodePropertyArray.getExample();
          comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.buildPageNodes = OASConfigNodePropertyBoolean.getExample();
          comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.buildClientLibs = OASConfigNodePropertyBoolean.getExample();
          comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.buildCanvasComponent = OASConfigNodePropertyBoolean.getExample();
        return comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWcmDesignimporterImplMobi) {           
            OASComDayCqWcmDesignimporterImplMobi comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties = (OASComDayCqWcmDesignimporterImplMobi) obj;
            return this.filepattern == comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.filepattern
                && this.deviceGroups == comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.deviceGroups
                && this.buildPageNodes == comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.buildPageNodes
                && this.buildClientLibs == comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.buildClientLibs
                && this.buildCanvasComponent == comDayCqWcmDesignimporterImplMobileCanvasBuilderImplProperties.buildCanvasComponent;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (filepattern == null ? 0 : System.hashCode(filepattern));
        hashCode = (17 * hashCode) + (deviceGroups == null ? 0 : System.hashCode(deviceGroups));
        hashCode = (17 * hashCode) + (buildPageNodes == null ? 0 : System.hashCode(buildPageNodes));
        hashCode = (17 * hashCode) + (buildClientLibs == null ? 0 : System.hashCode(buildClientLibs));
        hashCode = (17 * hashCode) + (buildCanvasComponent == null ? 0 : System.hashCode(buildCanvasComponent));
        return hashCode;
    }
}

