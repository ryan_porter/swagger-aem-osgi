/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWcmFoundationImplPageImpr
 */
public class OASComDayCqWcmFoundationImplPageImpr implements OAS.MappedProperties {
    /**
     * Get slingAuthRequirements
     * @return slingAuthRequirements
     */
    public OASConfigNodePropertyString slingAuthRequirements { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'sling.auth.requirements' => 'slingAuthRequirements'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqWcmFoundationImplPageImpr getExample() {
        OASComDayCqWcmFoundationImplPageImpr comDayCqWcmFoundationImplPageImpressionsTrackerProperties = new OASComDayCqWcmFoundationImplPageImpr();
          comDayCqWcmFoundationImplPageImpressionsTrackerProperties.slingAuthRequirements = OASConfigNodePropertyString.getExample();
        return comDayCqWcmFoundationImplPageImpressionsTrackerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWcmFoundationImplPageImpr) {           
            OASComDayCqWcmFoundationImplPageImpr comDayCqWcmFoundationImplPageImpressionsTrackerProperties = (OASComDayCqWcmFoundationImplPageImpr) obj;
            return this.slingAuthRequirements == comDayCqWcmFoundationImplPageImpressionsTrackerProperties.slingAuthRequirements;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (slingAuthRequirements == null ? 0 : System.hashCode(slingAuthRequirements));
        return hashCode;
    }
}

