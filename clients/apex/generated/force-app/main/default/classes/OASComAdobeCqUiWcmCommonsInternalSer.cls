/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqUiWcmCommonsInternalSer
 */
public class OASComAdobeCqUiWcmCommonsInternalSer implements OAS.MappedProperties {
    /**
     * Get resourceTypes
     * @return resourceTypes
     */
    public OASConfigNodePropertyArray resourceTypes { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'resource.types' => 'resourceTypes'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqUiWcmCommonsInternalSer getExample() {
        OASComAdobeCqUiWcmCommonsInternalSer comAdobeCqUiWcmCommonsInternalServletsRteRTEFilterServletFactProperties = new OASComAdobeCqUiWcmCommonsInternalSer();
          comAdobeCqUiWcmCommonsInternalServletsRteRTEFilterServletFactProperties.resourceTypes = OASConfigNodePropertyArray.getExample();
        return comAdobeCqUiWcmCommonsInternalServletsRteRTEFilterServletFactProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqUiWcmCommonsInternalSer) {           
            OASComAdobeCqUiWcmCommonsInternalSer comAdobeCqUiWcmCommonsInternalServletsRteRTEFilterServletFactProperties = (OASComAdobeCqUiWcmCommonsInternalSer) obj;
            return this.resourceTypes == comAdobeCqUiWcmCommonsInternalServletsRteRTEFilterServletFactProperties.resourceTypes;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (resourceTypes == null ? 0 : System.hashCode(resourceTypes));
        return hashCode;
    }
}

