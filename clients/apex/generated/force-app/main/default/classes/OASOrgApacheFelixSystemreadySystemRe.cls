/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheFelixSystemreadySystemRe
 */
public class OASOrgApacheFelixSystemreadySystemRe implements OAS.MappedProperties {
    /**
     * Get pollInterval
     * @return pollInterval
     */
    public OASConfigNodePropertyInteger pollInterval { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'poll.interval' => 'pollInterval'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheFelixSystemreadySystemRe getExample() {
        OASOrgApacheFelixSystemreadySystemRe orgApacheFelixSystemreadySystemReadyMonitorProperties = new OASOrgApacheFelixSystemreadySystemRe();
          orgApacheFelixSystemreadySystemReadyMonitorProperties.pollInterval = OASConfigNodePropertyInteger.getExample();
        return orgApacheFelixSystemreadySystemReadyMonitorProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheFelixSystemreadySystemRe) {           
            OASOrgApacheFelixSystemreadySystemRe orgApacheFelixSystemreadySystemReadyMonitorProperties = (OASOrgApacheFelixSystemreadySystemRe) obj;
            return this.pollInterval == orgApacheFelixSystemreadySystemReadyMonitorProperties.pollInterval;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (pollInterval == null ? 0 : System.hashCode(pollInterval));
        return hashCode;
    }
}

