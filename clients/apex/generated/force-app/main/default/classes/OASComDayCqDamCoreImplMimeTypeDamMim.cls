/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqDamCoreImplMimeTypeDamMim
 */
public class OASComDayCqDamCoreImplMimeTypeDamMim implements OAS.MappedProperties {
    /**
     * Get cqDamDetectAssetMimeFromContent
     * @return cqDamDetectAssetMimeFromContent
     */
    public OASConfigNodePropertyBoolean cqDamDetectAssetMimeFromContent { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'cq.dam.detect.asset.mime.from.content' => 'cqDamDetectAssetMimeFromContent'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqDamCoreImplMimeTypeDamMim getExample() {
        OASComDayCqDamCoreImplMimeTypeDamMim comDayCqDamCoreImplMimeTypeDamMimeTypeServiceImplProperties = new OASComDayCqDamCoreImplMimeTypeDamMim();
          comDayCqDamCoreImplMimeTypeDamMimeTypeServiceImplProperties.cqDamDetectAssetMimeFromContent = OASConfigNodePropertyBoolean.getExample();
        return comDayCqDamCoreImplMimeTypeDamMimeTypeServiceImplProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqDamCoreImplMimeTypeDamMim) {           
            OASComDayCqDamCoreImplMimeTypeDamMim comDayCqDamCoreImplMimeTypeDamMimeTypeServiceImplProperties = (OASComDayCqDamCoreImplMimeTypeDamMim) obj;
            return this.cqDamDetectAssetMimeFromContent == comDayCqDamCoreImplMimeTypeDamMimeTypeServiceImplProperties.cqDamDetectAssetMimeFromContent;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (cqDamDetectAssetMimeFromContent == null ? 0 : System.hashCode(cqDamDetectAssetMimeFromContent));
        return hashCode;
    }
}

