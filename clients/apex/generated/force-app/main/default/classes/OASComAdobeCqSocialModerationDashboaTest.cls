@isTest
private class OASComAdobeCqSocialModerationDashboaTest {
    @isTest
    private static void equalsSameInstance() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2 = comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1;
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3 = new OASComAdobeCqSocialModerationDashboa();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4 = comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3;

        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3 = new OASComAdobeCqSocialModerationDashboa();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4 = new OASComAdobeCqSocialModerationDashboa();

        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2 = new OASComAdobeCqSocialModerationDashboa();

        System.assertEquals(false, comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.equals('foo'));
        System.assertEquals(false, comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2 = new OASComAdobeCqSocialModerationDashboa();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3;

        System.assertEquals(false, comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3));
        System.assertEquals(false, comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2 = new OASComAdobeCqSocialModerationDashboa();

        System.assertEquals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.hashCode(), comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.hashCode());
        System.assertEquals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.hashCode(), comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2 = OASComAdobeCqSocialModerationDashboa.getExample();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3 = new OASComAdobeCqSocialModerationDashboa();
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4 = new OASComAdobeCqSocialModerationDashboa();

        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2));
        System.assert(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3.equals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4));
        System.assertEquals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties1.hashCode(), comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties2.hashCode());
        System.assertEquals(comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties3.hashCode(), comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties4.hashCode());
    }

    @isTest
    private static void maintainRenamedProperties() {
        OASComAdobeCqSocialModerationDashboa comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties = new OASComAdobeCqSocialModerationDashboa();
        Map<String, String> propertyMappings = comAdobeCqSocialModerationDashboardInternalImplFilterGroupSociProperties.getPropertyMappings();
        System.assertEquals('resourceTypeFilters', propertyMappings.get('resourceType.filters'));
    }
}
