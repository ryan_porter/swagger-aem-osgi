/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqWorkflowImplEmailTaskEMai
 */
public class OASComDayCqWorkflowImplEmailTaskEMai implements OAS.MappedProperties {
    /**
     * Get notifyOnupdate
     * @return notifyOnupdate
     */
    public OASConfigNodePropertyBoolean notifyOnupdate { get; set; }

    /**
     * Get notifyOncomplete
     * @return notifyOncomplete
     */
    public OASConfigNodePropertyBoolean notifyOncomplete { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'notify.onupdate' => 'notifyOnupdate',
        'notify.oncomplete' => 'notifyOncomplete'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqWorkflowImplEmailTaskEMai getExample() {
        OASComDayCqWorkflowImplEmailTaskEMai comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties = new OASComDayCqWorkflowImplEmailTaskEMai();
          comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties.notifyOnupdate = OASConfigNodePropertyBoolean.getExample();
          comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties.notifyOncomplete = OASConfigNodePropertyBoolean.getExample();
        return comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqWorkflowImplEmailTaskEMai) {           
            OASComDayCqWorkflowImplEmailTaskEMai comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties = (OASComDayCqWorkflowImplEmailTaskEMai) obj;
            return this.notifyOnupdate == comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties.notifyOnupdate
                && this.notifyOncomplete == comDayCqWorkflowImplEmailTaskEMailNotificationServiceProperties.notifyOncomplete;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (notifyOnupdate == null ? 0 : System.hashCode(notifyOnupdate));
        hashCode = (17 * hashCode) + (notifyOncomplete == null ? 0 : System.hashCode(notifyOncomplete));
        return hashCode;
    }
}

