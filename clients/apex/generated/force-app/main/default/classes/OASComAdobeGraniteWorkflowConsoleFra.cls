/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteWorkflowConsoleFra
 */
public class OASComAdobeGraniteWorkflowConsoleFra {
    /**
     * Get enabled
     * @return enabled
     */
    public OASConfigNodePropertyBoolean enabled { get; set; }

    public static OASComAdobeGraniteWorkflowConsoleFra getExample() {
        OASComAdobeGraniteWorkflowConsoleFra comAdobeGraniteWorkflowConsoleFragsWorkflowWithdrawFeatureProperties = new OASComAdobeGraniteWorkflowConsoleFra();
          comAdobeGraniteWorkflowConsoleFragsWorkflowWithdrawFeatureProperties.enabled = OASConfigNodePropertyBoolean.getExample();
        return comAdobeGraniteWorkflowConsoleFragsWorkflowWithdrawFeatureProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteWorkflowConsoleFra) {           
            OASComAdobeGraniteWorkflowConsoleFra comAdobeGraniteWorkflowConsoleFragsWorkflowWithdrawFeatureProperties = (OASComAdobeGraniteWorkflowConsoleFra) obj;
            return this.enabled == comAdobeGraniteWorkflowConsoleFragsWorkflowWithdrawFeatureProperties.enabled;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (enabled == null ? 0 : System.hashCode(enabled));
        return hashCode;
    }
}

