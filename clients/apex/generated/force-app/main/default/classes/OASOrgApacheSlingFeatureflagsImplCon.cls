/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheSlingFeatureflagsImplCon
 */
public class OASOrgApacheSlingFeatureflagsImplCon {
    /**
     * Get name
     * @return name
     */
    public OASConfigNodePropertyString name { get; set; }

    /**
     * Get description
     * @return description
     */
    public OASConfigNodePropertyString description { get; set; }

    /**
     * Get enabled
     * @return enabled
     */
    public OASConfigNodePropertyBoolean enabled { get; set; }

    public static OASOrgApacheSlingFeatureflagsImplCon getExample() {
        OASOrgApacheSlingFeatureflagsImplCon orgApacheSlingFeatureflagsImplConfiguredFeatureProperties = new OASOrgApacheSlingFeatureflagsImplCon();
          orgApacheSlingFeatureflagsImplConfiguredFeatureProperties.name = OASConfigNodePropertyString.getExample();
          orgApacheSlingFeatureflagsImplConfiguredFeatureProperties.description = OASConfigNodePropertyString.getExample();
          orgApacheSlingFeatureflagsImplConfiguredFeatureProperties.enabled = OASConfigNodePropertyBoolean.getExample();
        return orgApacheSlingFeatureflagsImplConfiguredFeatureProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheSlingFeatureflagsImplCon) {           
            OASOrgApacheSlingFeatureflagsImplCon orgApacheSlingFeatureflagsImplConfiguredFeatureProperties = (OASOrgApacheSlingFeatureflagsImplCon) obj;
            return this.name == orgApacheSlingFeatureflagsImplConfiguredFeatureProperties.name
                && this.description == orgApacheSlingFeatureflagsImplConfiguredFeatureProperties.description
                && this.enabled == orgApacheSlingFeatureflagsImplConfiguredFeatureProperties.enabled;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (name == null ? 0 : System.hashCode(name));
        hashCode = (17 * hashCode) + (description == null ? 0 : System.hashCode(description));
        hashCode = (17 * hashCode) + (enabled == null ? 0 : System.hashCode(enabled));
        return hashCode;
    }
}

