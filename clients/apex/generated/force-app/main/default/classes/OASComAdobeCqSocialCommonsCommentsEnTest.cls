@isTest
private class OASComAdobeCqSocialCommonsCommentsEnTest {
    @isTest
    private static void equalsSameInstance() {
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2 = comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1;
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3 = new OASComAdobeCqSocialCommonsCommentsEn();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4 = comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3;

        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3 = new OASComAdobeCqSocialCommonsCommentsEn();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4 = new OASComAdobeCqSocialCommonsCommentsEn();

        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2 = new OASComAdobeCqSocialCommonsCommentsEn();

        System.assertEquals(false, comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.equals('foo'));
        System.assertEquals(false, comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2 = new OASComAdobeCqSocialCommonsCommentsEn();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3;

        System.assertEquals(false, comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3));
        System.assertEquals(false, comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2 = new OASComAdobeCqSocialCommonsCommentsEn();

        System.assertEquals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.hashCode(), comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.hashCode());
        System.assertEquals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.hashCode(), comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2 = OASComAdobeCqSocialCommonsCommentsEn.getExample();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3 = new OASComAdobeCqSocialCommonsCommentsEn();
        OASComAdobeCqSocialCommonsCommentsEn comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4 = new OASComAdobeCqSocialCommonsCommentsEn();

        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2));
        System.assert(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3.equals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4));
        System.assertEquals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties1.hashCode(), comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties2.hashCode());
        System.assertEquals(comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties3.hashCode(), comAdobeCqSocialCommonsCommentsEndpointsImplTranslationOperatiProperties4.hashCode());
    }
}
