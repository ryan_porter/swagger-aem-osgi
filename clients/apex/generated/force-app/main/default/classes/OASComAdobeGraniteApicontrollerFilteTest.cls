@isTest
private class OASComAdobeGraniteApicontrollerFilteTest {
    @isTest
    private static void equalsSameInstance() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2 = comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1;
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3 = new OASComAdobeGraniteApicontrollerFilte();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4 = comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3;

        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3));
    }

    @isTest
    private static void equalsIdenticalInstance() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3 = new OASComAdobeGraniteApicontrollerFilte();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4 = new OASComAdobeGraniteApicontrollerFilte();

        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3));
    }

    @isTest
    private static void notEqualsDifferentType() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2 = new OASComAdobeGraniteApicontrollerFilte();

        System.assertEquals(false, comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.equals('foo'));
        System.assertEquals(false, comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.equals('foo'));
    }

    @isTest
    private static void notEqualsNull() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2 = new OASComAdobeGraniteApicontrollerFilte();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3;

        System.assertEquals(false, comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3));
        System.assertEquals(false, comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3));
    }

    @isTest
    private static void consistentHashCodeValue() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2 = new OASComAdobeGraniteApicontrollerFilte();

        System.assertEquals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.hashCode(), comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.hashCode());
        System.assertEquals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.hashCode(), comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.hashCode());
    }

    @isTest
    private static void equalInstancesHaveSameHashCode() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2 = OASComAdobeGraniteApicontrollerFilte.getExample();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3 = new OASComAdobeGraniteApicontrollerFilte();
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4 = new OASComAdobeGraniteApicontrollerFilte();

        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2));
        System.assert(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3.equals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4));
        System.assertEquals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties1.hashCode(), comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties2.hashCode());
        System.assertEquals(comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties3.hashCode(), comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties4.hashCode());
    }

    @isTest
    private static void maintainRenamedProperties() {
        OASComAdobeGraniteApicontrollerFilte comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties = new OASComAdobeGraniteApicontrollerFilte();
        Map<String, String> propertyMappings = comAdobeGraniteApicontrollerFilterResolverHookFactoryProperties.getPropertyMappings();
        System.assertEquals('comAdobeCqCdnCdnRewriter', propertyMappings.get('com.adobe.cq.cdn.cdn-rewriter'));
        System.assertEquals('comAdobeCqCloudConfigComponents', propertyMappings.get('com.adobe.cq.cloud-config.components'));
        System.assertEquals('comAdobeCqCloudConfigCore', propertyMappings.get('com.adobe.cq.cloud-config.core'));
        System.assertEquals('comAdobeCqCloudConfigUi', propertyMappings.get('com.adobe.cq.cloud-config.ui'));
        System.assertEquals('comAdobeCqComAdobeCqEditor', propertyMappings.get('com.adobe.cq.com.adobe.cq.editor'));
        System.assertEquals('comAdobeCqComAdobeCqProjectsCore', propertyMappings.get('com.adobe.cq.com.adobe.cq.projects.core'));
        System.assertEquals('comAdobeCqComAdobeCqProjectsWcmCore', propertyMappings.get('com.adobe.cq.com.adobe.cq.projects.wcm.core'));
        System.assertEquals('comAdobeCqComAdobeCqUiCommons', propertyMappings.get('com.adobe.cq.com.adobe.cq.ui.commons'));
        System.assertEquals('comAdobeCqComAdobeCqWcmStyle', propertyMappings.get('com.adobe.cq.com.adobe.cq.wcm.style'));
        System.assertEquals('comAdobeCqCqActivitymapIntegration', propertyMappings.get('com.adobe.cq.cq-activitymap-integration'));
        System.assertEquals('comAdobeCqCqContexthubCommons', propertyMappings.get('com.adobe.cq.cq-contexthub-commons'));
        System.assertEquals('comAdobeCqCqDtm', propertyMappings.get('com.adobe.cq.cq-dtm'));
        System.assertEquals('comAdobeCqCqHealthcheck', propertyMappings.get('com.adobe.cq.cq-healthcheck'));
        System.assertEquals('comAdobeCqCqMultisiteTargeting', propertyMappings.get('com.adobe.cq.cq-multisite-targeting'));
        System.assertEquals('comAdobeCqCqPreUpgradeCleanup', propertyMappings.get('com.adobe.cq.cq-pre-upgrade-cleanup'));
        System.assertEquals('comAdobeCqCqProductInfoProvider', propertyMappings.get('com.adobe.cq.cq-product-info-provider'));
        System.assertEquals('comAdobeCqCqRestSites', propertyMappings.get('com.adobe.cq.cq-rest-sites'));
        System.assertEquals('comAdobeCqCqSecurityHc', propertyMappings.get('com.adobe.cq.cq-security-hc'));
        System.assertEquals('comAdobeCqDamCqDamSvgHandler', propertyMappings.get('com.adobe.cq.dam.cq-dam-svg-handler'));
        System.assertEquals('comAdobeCqDamCqScene7Imaging', propertyMappings.get('com.adobe.cq.dam.cq-scene7-imaging'));
        System.assertEquals('comAdobeCqDtmReactorCore', propertyMappings.get('com.adobe.cq.dtm-reactor.core'));
        System.assertEquals('comAdobeCqDtmReactorUi', propertyMappings.get('com.adobe.cq.dtm-reactor.ui'));
        System.assertEquals('comAdobeCqExpJspelResolver', propertyMappings.get('com.adobe.cq.exp-jspel-resolver'));
        System.assertEquals('comAdobeCqInboxCqInbox', propertyMappings.get('com.adobe.cq.inbox.cq-inbox'));
        System.assertEquals('comAdobeCqJsonSchemaParser', propertyMappings.get('com.adobe.cq.json-schema-parser'));
        System.assertEquals('comAdobeCqMediaCqMediaPublishingDpsFpCore', propertyMappings.get('com.adobe.cq.media.cq-media-publishing-dps-fp-core'));
        System.assertEquals('comAdobeCqMobileCqMobileCaas', propertyMappings.get('com.adobe.cq.mobile.cq-mobile-caas'));
        System.assertEquals('comAdobeCqMobileCqMobileIndexBuilder', propertyMappings.get('com.adobe.cq.mobile.cq-mobile-index-builder'));
        System.assertEquals('comAdobeCqMobileCqMobilePhonegapBuild', propertyMappings.get('com.adobe.cq.mobile.cq-mobile-phonegap-build'));
        System.assertEquals('comAdobeCqMyspell', propertyMappings.get('com.adobe.cq.myspell'));
        System.assertEquals('comAdobeCqSampleWeRetailCore', propertyMappings.get('com.adobe.cq.sample.we.retail.core'));
        System.assertEquals('comAdobeCqScreensComAdobeCqScreensDcc', propertyMappings.get('com.adobe.cq.screens.com.adobe.cq.screens.dcc'));
        System.assertEquals('comAdobeCqScreensComAdobeCqScreensMqCore', propertyMappings.get('com.adobe.cq.screens.com.adobe.cq.screens.mq.core'));
        System.assertEquals('comAdobeCqSocialCqSocialAsProvider', propertyMappings.get('com.adobe.cq.social.cq-social-as-provider'));
        System.assertEquals('comAdobeCqSocialCqSocialBadgingBasicImpl', propertyMappings.get('com.adobe.cq.social.cq-social-badging-basic-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialBadgingImpl', propertyMappings.get('com.adobe.cq.social.cq-social-badging-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialCalendarImpl', propertyMappings.get('com.adobe.cq.social.cq-social-calendar-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialContentFragmentsImpl', propertyMappings.get('com.adobe.cq.social.cq-social-content-fragments-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialEnablementImpl', propertyMappings.get('com.adobe.cq.social.cq-social-enablement-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialGraphImpl', propertyMappings.get('com.adobe.cq.social.cq-social-graph-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialIdeationImpl', propertyMappings.get('com.adobe.cq.social.cq-social-ideation-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialJcrProvider', propertyMappings.get('com.adobe.cq.social.cq-social-jcr-provider'));
        System.assertEquals('comAdobeCqSocialCqSocialMembersImpl', propertyMappings.get('com.adobe.cq.social.cq-social-members-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialMsProvider', propertyMappings.get('com.adobe.cq.social.cq-social-ms-provider'));
        System.assertEquals('comAdobeCqSocialCqSocialNotificationsChannelsWeb', propertyMappings.get('com.adobe.cq.social.cq-social-notifications-channels-web'));
        System.assertEquals('comAdobeCqSocialCqSocialNotificationsImpl', propertyMappings.get('com.adobe.cq.social.cq-social-notifications-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialRdbProvider', propertyMappings.get('com.adobe.cq.social.cq-social-rdb-provider'));
        System.assertEquals('comAdobeCqSocialCqSocialScfImpl', propertyMappings.get('com.adobe.cq.social.cq-social-scf-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialScoringBasicImpl', propertyMappings.get('com.adobe.cq.social.cq-social-scoring-basic-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialScoringImpl', propertyMappings.get('com.adobe.cq.social.cq-social-scoring-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialServiceusersImpl', propertyMappings.get('com.adobe.cq.social.cq-social-serviceusers-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialSrpImpl', propertyMappings.get('com.adobe.cq.social.cq-social-srp-impl'));
        System.assertEquals('comAdobeCqSocialCqSocialUgcbaseImpl', propertyMappings.get('com.adobe.cq.social.cq-social-ugcbase-impl'));
        System.assertEquals('comAdobeDamCqDamCfmImpl', propertyMappings.get('com.adobe.dam.cq-dam-cfm-impl'));
        System.assertEquals('comAdobeFormsFoundationFormsFoundationBase', propertyMappings.get('com.adobe.forms.foundation-forms-foundation-base'));
        System.assertEquals('comAdobeGraniteApicontroller', propertyMappings.get('com.adobe.granite.apicontroller'));
        System.assertEquals('comAdobeGraniteAssetCore', propertyMappings.get('com.adobe.granite.asset.core'));
        System.assertEquals('comAdobeGraniteAuthSso', propertyMappings.get('com.adobe.granite.auth.sso'));
        System.assertEquals('comAdobeGraniteBundlesHcImpl', propertyMappings.get('com.adobe.granite.bundles.hc.impl'));
        System.assertEquals('comAdobeGraniteCompatRouter', propertyMappings.get('com.adobe.granite.compat-router'));
        System.assertEquals('comAdobeGraniteConf', propertyMappings.get('com.adobe.granite.conf'));
        System.assertEquals('comAdobeGraniteConfUiCore', propertyMappings.get('com.adobe.granite.conf.ui.core'));
        System.assertEquals('comAdobeGraniteCors', propertyMappings.get('com.adobe.granite.cors'));
        System.assertEquals('comAdobeGraniteCrxExplorer', propertyMappings.get('com.adobe.granite.crx-explorer'));
        System.assertEquals('comAdobeGraniteCrxdeLite', propertyMappings.get('com.adobe.granite.crxde-lite'));
        System.assertEquals('comAdobeGraniteCryptoConfig', propertyMappings.get('com.adobe.granite.crypto.config'));
        System.assertEquals('comAdobeGraniteCryptoExtension', propertyMappings.get('com.adobe.granite.crypto.extension'));
        System.assertEquals('comAdobeGraniteCryptoFile', propertyMappings.get('com.adobe.granite.crypto.file'));
        System.assertEquals('comAdobeGraniteCryptoJcr', propertyMappings.get('com.adobe.granite.crypto.jcr'));
        System.assertEquals('comAdobeGraniteCsrf', propertyMappings.get('com.adobe.granite.csrf'));
        System.assertEquals('comAdobeGraniteDistributionCore', propertyMappings.get('com.adobe.granite.distribution.core'));
        System.assertEquals('comAdobeGraniteDropwizardMetrics', propertyMappings.get('com.adobe.granite.dropwizard.metrics'));
        System.assertEquals('comAdobeGraniteFragsImpl', propertyMappings.get('com.adobe.granite.frags.impl'));
        System.assertEquals('comAdobeGraniteGibson', propertyMappings.get('com.adobe.granite.gibson'));
        System.assertEquals('comAdobeGraniteInfocollector', propertyMappings.get('com.adobe.granite.infocollector'));
        System.assertEquals('comAdobeGraniteInstallerFactoryPackages', propertyMappings.get('com.adobe.granite.installer.factory.packages'));
        System.assertEquals('comAdobeGraniteJettySsl', propertyMappings.get('com.adobe.granite.jetty.ssl'));
        System.assertEquals('comAdobeGraniteJobsAsync', propertyMappings.get('com.adobe.granite.jobs.async'));
        System.assertEquals('comAdobeGraniteMaintenanceOak', propertyMappings.get('com.adobe.granite.maintenance.oak'));
        System.assertEquals('comAdobeGraniteMonitoringCore', propertyMappings.get('com.adobe.granite.monitoring.core'));
        System.assertEquals('comAdobeGraniteQueries', propertyMappings.get('com.adobe.granite.queries'));
        System.assertEquals('comAdobeGraniteReplicationHcImpl', propertyMappings.get('com.adobe.granite.replication.hc.impl'));
        System.assertEquals('comAdobeGraniteRepositoryChecker', propertyMappings.get('com.adobe.granite.repository.checker'));
        System.assertEquals('comAdobeGraniteRepositoryHcImpl', propertyMappings.get('com.adobe.granite.repository.hc.impl'));
        System.assertEquals('comAdobeGraniteRestAssets', propertyMappings.get('com.adobe.granite.rest.assets'));
        System.assertEquals('comAdobeGraniteSecurityUi', propertyMappings.get('com.adobe.granite.security.ui'));
        System.assertEquals('comAdobeGraniteStartup', propertyMappings.get('com.adobe.granite.startup'));
        System.assertEquals('comAdobeGraniteTagsoup', propertyMappings.get('com.adobe.granite.tagsoup'));
        System.assertEquals('comAdobeGraniteTaskmanagementCore', propertyMappings.get('com.adobe.granite.taskmanagement.core'));
        System.assertEquals('comAdobeGraniteTaskmanagementWorkflow', propertyMappings.get('com.adobe.granite.taskmanagement.workflow'));
        System.assertEquals('comAdobeGraniteUiClientlibsCompilerLess', propertyMappings.get('com.adobe.granite.ui.clientlibs.compiler.less'));
        System.assertEquals('comAdobeGraniteUiClientlibsProcessorGcc', propertyMappings.get('com.adobe.granite.ui.clientlibs.processor.gcc'));
        System.assertEquals('comAdobeGraniteWebconsolePlugins', propertyMappings.get('com.adobe.granite.webconsole.plugins'));
        System.assertEquals('comAdobeGraniteWorkflowConsole', propertyMappings.get('com.adobe.granite.workflow.console'));
        System.assertEquals('comAdobeXmpWorkerFilesNativeFragmentLinux', propertyMappings.get('com.adobe.xmp.worker.files.native.fragment.linux'));
        System.assertEquals('comAdobeXmpWorkerFilesNativeFragmentMacosx', propertyMappings.get('com.adobe.xmp.worker.files.native.fragment.macosx'));
        System.assertEquals('comAdobeXmpWorkerFilesNativeFragmentWin', propertyMappings.get('com.adobe.xmp.worker.files.native.fragment.win'));
        System.assertEquals('comDayCommonsOsgiWrapperSimpleJndi', propertyMappings.get('com.day.commons.osgi.wrapper.simple-jndi'));
        System.assertEquals('comDayCqCqAuthhandler', propertyMappings.get('com.day.cq.cq-authhandler'));
        System.assertEquals('comDayCqCqCompatConfigupdate', propertyMappings.get('com.day.cq.cq-compat-configupdate'));
        System.assertEquals('comDayCqCqLicensebranding', propertyMappings.get('com.day.cq.cq-licensebranding'));
        System.assertEquals('comDayCqCqNotifcationImpl', propertyMappings.get('com.day.cq.cq-notifcation-impl'));
        System.assertEquals('comDayCqCqReplicationAudit', propertyMappings.get('com.day.cq.cq-replication-audit'));
        System.assertEquals('comDayCqCqSearchExt', propertyMappings.get('com.day.cq.cq-search-ext'));
        System.assertEquals('comDayCqDamCqDamAnnotationPrint', propertyMappings.get('com.day.cq.dam.cq-dam-annotation-print'));
        System.assertEquals('comDayCqDamCqDamAssetUsage', propertyMappings.get('com.day.cq.dam.cq-dam-asset-usage'));
        System.assertEquals('comDayCqDamCqDamS7dam', propertyMappings.get('com.day.cq.dam.cq-dam-s7dam'));
        System.assertEquals('comDayCqDamCqDamSimilaritysearch', propertyMappings.get('com.day.cq.dam.cq-dam-similaritysearch'));
        System.assertEquals('comDayCqDamDamWebdavSupport', propertyMappings.get('com.day.cq.dam.dam-webdav-support'));
        System.assertEquals('comDayCqPreUpgradeTasks', propertyMappings.get('com.day.cq.pre-upgrade-tasks'));
        System.assertEquals('comDayCqReplicationExtensions', propertyMappings.get('com.day.cq.replication.extensions'));
        System.assertEquals('comDayCqWcmCqMsmCore', propertyMappings.get('com.day.cq.wcm.cq-msm-core'));
        System.assertEquals('comDayCqWcmCqWcmTranslation', propertyMappings.get('com.day.cq.wcm.cq-wcm-translation'));
        System.assertEquals('dayCommonsJrawio', propertyMappings.get('day-commons-jrawio'));
        System.assertEquals('orgApacheAriesJmxWhiteboard', propertyMappings.get('org.apache.aries.jmx.whiteboard'));
        System.assertEquals('orgApacheFelixHttpSslfilter', propertyMappings.get('org.apache.felix.http.sslfilter'));
        System.assertEquals('orgApacheFelixOrgApacheFelixThreaddump', propertyMappings.get('org.apache.felix.org.apache.felix.threaddump'));
        System.assertEquals('orgApacheFelixWebconsolePluginsDs', propertyMappings.get('org.apache.felix.webconsole.plugins.ds'));
        System.assertEquals('orgApacheFelixWebconsolePluginsEvent', propertyMappings.get('org.apache.felix.webconsole.plugins.event'));
        System.assertEquals('orgApacheFelixWebconsolePluginsMemoryusage', propertyMappings.get('org.apache.felix.webconsole.plugins.memoryusage'));
        System.assertEquals('orgApacheFelixWebconsolePluginsPackageadmin', propertyMappings.get('org.apache.felix.webconsole.plugins.packageadmin'));
        System.assertEquals('orgApacheJackrabbitOakAuthLdap', propertyMappings.get('org.apache.jackrabbit.oak-auth-ldap'));
        System.assertEquals('orgApacheJackrabbitOakSegmentTar', propertyMappings.get('org.apache.jackrabbit.oak-segment-tar'));
        System.assertEquals('orgApacheJackrabbitOakSolrOsgi', propertyMappings.get('org.apache.jackrabbit.oak-solr-osgi'));
        System.assertEquals('orgApacheSlingBundleresourceImpl', propertyMappings.get('org.apache.sling.bundleresource.impl'));
        System.assertEquals('orgApacheSlingCommonsFsclassloader', propertyMappings.get('org.apache.sling.commons.fsclassloader'));
        System.assertEquals('orgApacheSlingCommonsLogWebconsole', propertyMappings.get('org.apache.sling.commons.log.webconsole'));
        System.assertEquals('orgApacheSlingDatasource', propertyMappings.get('org.apache.sling.datasource'));
        System.assertEquals('orgApacheSlingDiscoveryBase', propertyMappings.get('org.apache.sling.discovery.base'));
        System.assertEquals('orgApacheSlingDiscoveryOak', propertyMappings.get('org.apache.sling.discovery.oak'));
        System.assertEquals('orgApacheSlingDiscoverySupport', propertyMappings.get('org.apache.sling.discovery.support'));
        System.assertEquals('orgApacheSlingDistributionApi', propertyMappings.get('org.apache.sling.distribution.api'));
        System.assertEquals('orgApacheSlingDistributionCore', propertyMappings.get('org.apache.sling.distribution.core'));
        System.assertEquals('orgApacheSlingExtensionsWebconsolesecurityprovider', propertyMappings.get('org.apache.sling.extensions.webconsolesecurityprovider'));
        System.assertEquals('orgApacheSlingHcWebconsole', propertyMappings.get('org.apache.sling.hc.webconsole'));
        System.assertEquals('orgApacheSlingInstallerConsole', propertyMappings.get('org.apache.sling.installer.console'));
        System.assertEquals('orgApacheSlingInstallerProviderFile', propertyMappings.get('org.apache.sling.installer.provider.file'));
        System.assertEquals('orgApacheSlingInstallerProviderJcr', propertyMappings.get('org.apache.sling.installer.provider.jcr'));
        System.assertEquals('orgApacheSlingJcrDavex', propertyMappings.get('org.apache.sling.jcr.davex'));
        System.assertEquals('orgApacheSlingJcrResourcesecurity', propertyMappings.get('org.apache.sling.jcr.resourcesecurity'));
        System.assertEquals('orgApacheSlingJmxProvider', propertyMappings.get('org.apache.sling.jmx.provider'));
        System.assertEquals('orgApacheSlingLaunchpadInstaller', propertyMappings.get('org.apache.sling.launchpad.installer'));
        System.assertEquals('orgApacheSlingModelsImpl', propertyMappings.get('org.apache.sling.models.impl'));
        System.assertEquals('orgApacheSlingRepoinitParser', propertyMappings.get('org.apache.sling.repoinit.parser'));
        System.assertEquals('orgApacheSlingResourceInventory', propertyMappings.get('org.apache.sling.resource.inventory'));
        System.assertEquals('orgApacheSlingResourceresolver', propertyMappings.get('org.apache.sling.resourceresolver'));
        System.assertEquals('orgApacheSlingScriptingJavascript', propertyMappings.get('org.apache.sling.scripting.javascript'));
        System.assertEquals('orgApacheSlingScriptingJst', propertyMappings.get('org.apache.sling.scripting.jst'));
        System.assertEquals('orgApacheSlingScriptingSightlyJsProvider', propertyMappings.get('org.apache.sling.scripting.sightly.js.provider'));
        System.assertEquals('orgApacheSlingScriptingSightlyModelsProvider', propertyMappings.get('org.apache.sling.scripting.sightly.models.provider'));
        System.assertEquals('orgApacheSlingSecurity', propertyMappings.get('org.apache.sling.security'));
        System.assertEquals('orgApacheSlingServletsCompat', propertyMappings.get('org.apache.sling.servlets.compat'));
        System.assertEquals('orgApacheSlingServletsGet', propertyMappings.get('org.apache.sling.servlets.get'));
        System.assertEquals('orgApacheSlingStartupfilterDisabler', propertyMappings.get('org.apache.sling.startupfilter.disabler'));
        System.assertEquals('orgApacheSlingTracer', propertyMappings.get('org.apache.sling.tracer'));
        System.assertEquals('weRetailClientAppCore', propertyMappings.get('we.retail.client.app.core'));
    }
}
