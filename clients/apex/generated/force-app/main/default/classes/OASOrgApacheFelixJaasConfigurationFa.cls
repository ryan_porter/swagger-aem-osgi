/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASOrgApacheFelixJaasConfigurationFa
 */
public class OASOrgApacheFelixJaasConfigurationFa implements OAS.MappedProperties {
    /**
     * Get jaasControlFlag
     * @return jaasControlFlag
     */
    public OASConfigNodePropertyDropDown jaasControlFlag { get; set; }

    /**
     * Get jaasRanking
     * @return jaasRanking
     */
    public OASConfigNodePropertyInteger jaasRanking { get; set; }

    /**
     * Get jaasRealmName
     * @return jaasRealmName
     */
    public OASConfigNodePropertyString jaasRealmName { get; set; }

    /**
     * Get jaasClassname
     * @return jaasClassname
     */
    public OASConfigNodePropertyString jaasClassname { get; set; }

    /**
     * Get jaasOptions
     * @return jaasOptions
     */
    public OASConfigNodePropertyArray jaasOptions { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'jaas.controlFlag' => 'jaasControlFlag',
        'jaas.ranking' => 'jaasRanking',
        'jaas.realmName' => 'jaasRealmName',
        'jaas.classname' => 'jaasClassname',
        'jaas.options' => 'jaasOptions'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASOrgApacheFelixJaasConfigurationFa getExample() {
        OASOrgApacheFelixJaasConfigurationFa orgApacheFelixJaasConfigurationFactoryProperties = new OASOrgApacheFelixJaasConfigurationFa();
          orgApacheFelixJaasConfigurationFactoryProperties.jaasControlFlag = OASConfigNodePropertyDropDown.getExample();
          orgApacheFelixJaasConfigurationFactoryProperties.jaasRanking = OASConfigNodePropertyInteger.getExample();
          orgApacheFelixJaasConfigurationFactoryProperties.jaasRealmName = OASConfigNodePropertyString.getExample();
          orgApacheFelixJaasConfigurationFactoryProperties.jaasClassname = OASConfigNodePropertyString.getExample();
          orgApacheFelixJaasConfigurationFactoryProperties.jaasOptions = OASConfigNodePropertyArray.getExample();
        return orgApacheFelixJaasConfigurationFactoryProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASOrgApacheFelixJaasConfigurationFa) {           
            OASOrgApacheFelixJaasConfigurationFa orgApacheFelixJaasConfigurationFactoryProperties = (OASOrgApacheFelixJaasConfigurationFa) obj;
            return this.jaasControlFlag == orgApacheFelixJaasConfigurationFactoryProperties.jaasControlFlag
                && this.jaasRanking == orgApacheFelixJaasConfigurationFactoryProperties.jaasRanking
                && this.jaasRealmName == orgApacheFelixJaasConfigurationFactoryProperties.jaasRealmName
                && this.jaasClassname == orgApacheFelixJaasConfigurationFactoryProperties.jaasClassname
                && this.jaasOptions == orgApacheFelixJaasConfigurationFactoryProperties.jaasOptions;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (jaasControlFlag == null ? 0 : System.hashCode(jaasControlFlag));
        hashCode = (17 * hashCode) + (jaasRanking == null ? 0 : System.hashCode(jaasRanking));
        hashCode = (17 * hashCode) + (jaasRealmName == null ? 0 : System.hashCode(jaasRealmName));
        hashCode = (17 * hashCode) + (jaasClassname == null ? 0 : System.hashCode(jaasClassname));
        hashCode = (17 * hashCode) + (jaasOptions == null ? 0 : System.hashCode(jaasOptions));
        return hashCode;
    }
}

