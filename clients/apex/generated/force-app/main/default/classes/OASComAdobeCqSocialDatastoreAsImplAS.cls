/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeCqSocialDatastoreAsImplAS
 */
public class OASComAdobeCqSocialDatastoreAsImplAS implements OAS.MappedProperties {
    /**
     * Get versionId
     * @return versionId
     */
    public OASConfigNodePropertyString versionId { get; set; }

    /**
     * Get cacheOn
     * @return cacheOn
     */
    public OASConfigNodePropertyBoolean cacheOn { get; set; }

    /**
     * Get concurrencyLevel
     * @return concurrencyLevel
     */
    public OASConfigNodePropertyInteger concurrencyLevel { get; set; }

    /**
     * Get cacheStartSize
     * @return cacheStartSize
     */
    public OASConfigNodePropertyInteger cacheStartSize { get; set; }

    /**
     * Get cacheTtl
     * @return cacheTtl
     */
    public OASConfigNodePropertyInteger cacheTtl { get; set; }

    /**
     * Get cacheSize
     * @return cacheSize
     */
    public OASConfigNodePropertyInteger cacheSize { get; set; }

    /**
     * Get timeLimit
     * @return timeLimit
     */
    public OASConfigNodePropertyInteger timeLimit { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'version.id' => 'versionId',
        'cache.on' => 'cacheOn',
        'concurrency.level' => 'concurrencyLevel',
        'cache.start.size' => 'cacheStartSize',
        'cache.ttl' => 'cacheTtl',
        'cache.size' => 'cacheSize',
        'time.limit' => 'timeLimit'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeCqSocialDatastoreAsImplAS getExample() {
        OASComAdobeCqSocialDatastoreAsImplAS comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties = new OASComAdobeCqSocialDatastoreAsImplAS();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.versionId = OASConfigNodePropertyString.getExample();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheOn = OASConfigNodePropertyBoolean.getExample();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.concurrencyLevel = OASConfigNodePropertyInteger.getExample();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheStartSize = OASConfigNodePropertyInteger.getExample();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheTtl = OASConfigNodePropertyInteger.getExample();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheSize = OASConfigNodePropertyInteger.getExample();
          comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.timeLimit = OASConfigNodePropertyInteger.getExample();
        return comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeCqSocialDatastoreAsImplAS) {           
            OASComAdobeCqSocialDatastoreAsImplAS comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties = (OASComAdobeCqSocialDatastoreAsImplAS) obj;
            return this.versionId == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.versionId
                && this.cacheOn == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheOn
                && this.concurrencyLevel == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.concurrencyLevel
                && this.cacheStartSize == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheStartSize
                && this.cacheTtl == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheTtl
                && this.cacheSize == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.cacheSize
                && this.timeLimit == comAdobeCqSocialDatastoreAsImplASResourceProviderFactoryProperties.timeLimit;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (versionId == null ? 0 : System.hashCode(versionId));
        hashCode = (17 * hashCode) + (cacheOn == null ? 0 : System.hashCode(cacheOn));
        hashCode = (17 * hashCode) + (concurrencyLevel == null ? 0 : System.hashCode(concurrencyLevel));
        hashCode = (17 * hashCode) + (cacheStartSize == null ? 0 : System.hashCode(cacheStartSize));
        hashCode = (17 * hashCode) + (cacheTtl == null ? 0 : System.hashCode(cacheTtl));
        hashCode = (17 * hashCode) + (cacheSize == null ? 0 : System.hashCode(cacheSize));
        hashCode = (17 * hashCode) + (timeLimit == null ? 0 : System.hashCode(timeLimit));
        return hashCode;
    }
}

