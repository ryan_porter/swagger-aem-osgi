/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteOffloadingImplTran
 */
public class OASComAdobeGraniteOffloadingImplTran implements OAS.MappedProperties {
    /**
     * Get defaultTransportAgentToWorkerPrefix
     * @return defaultTransportAgentToWorkerPrefix
     */
    public OASConfigNodePropertyString defaultTransportAgentToWorkerPrefix { get; set; }

    /**
     * Get defaultTransportAgentToMasterPrefix
     * @return defaultTransportAgentToMasterPrefix
     */
    public OASConfigNodePropertyString defaultTransportAgentToMasterPrefix { get; set; }

    /**
     * Get defaultTransportInputPackage
     * @return defaultTransportInputPackage
     */
    public OASConfigNodePropertyString defaultTransportInputPackage { get; set; }

    /**
     * Get defaultTransportOutputPackage
     * @return defaultTransportOutputPackage
     */
    public OASConfigNodePropertyString defaultTransportOutputPackage { get; set; }

    /**
     * Get defaultTransportReplicationSynchronous
     * @return defaultTransportReplicationSynchronous
     */
    public OASConfigNodePropertyBoolean defaultTransportReplicationSynchronous { get; set; }

    /**
     * Get defaultTransportContentpackage
     * @return defaultTransportContentpackage
     */
    public OASConfigNodePropertyBoolean defaultTransportContentpackage { get; set; }

    /**
     * Get offloadingTransporterDefaultEnabled
     * @return offloadingTransporterDefaultEnabled
     */
    public OASConfigNodePropertyBoolean offloadingTransporterDefaultEnabled { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'default.transport.agent-to-worker.prefix' => 'defaultTransportAgentToWorkerPrefix',
        'default.transport.agent-to-master.prefix' => 'defaultTransportAgentToMasterPrefix',
        'default.transport.input.package' => 'defaultTransportInputPackage',
        'default.transport.output.package' => 'defaultTransportOutputPackage',
        'default.transport.replication.synchronous' => 'defaultTransportReplicationSynchronous',
        'default.transport.contentpackage' => 'defaultTransportContentpackage',
        'offloading.transporter.default.enabled' => 'offloadingTransporterDefaultEnabled'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComAdobeGraniteOffloadingImplTran getExample() {
        OASComAdobeGraniteOffloadingImplTran comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties = new OASComAdobeGraniteOffloadingImplTran();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportAgentToWorkerPrefix = OASConfigNodePropertyString.getExample();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportAgentToMasterPrefix = OASConfigNodePropertyString.getExample();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportInputPackage = OASConfigNodePropertyString.getExample();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportOutputPackage = OASConfigNodePropertyString.getExample();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportReplicationSynchronous = OASConfigNodePropertyBoolean.getExample();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportContentpackage = OASConfigNodePropertyBoolean.getExample();
          comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.offloadingTransporterDefaultEnabled = OASConfigNodePropertyBoolean.getExample();
        return comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteOffloadingImplTran) {           
            OASComAdobeGraniteOffloadingImplTran comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties = (OASComAdobeGraniteOffloadingImplTran) obj;
            return this.defaultTransportAgentToWorkerPrefix == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportAgentToWorkerPrefix
                && this.defaultTransportAgentToMasterPrefix == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportAgentToMasterPrefix
                && this.defaultTransportInputPackage == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportInputPackage
                && this.defaultTransportOutputPackage == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportOutputPackage
                && this.defaultTransportReplicationSynchronous == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportReplicationSynchronous
                && this.defaultTransportContentpackage == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.defaultTransportContentpackage
                && this.offloadingTransporterDefaultEnabled == comAdobeGraniteOffloadingImplTransporterOffloadingDefaultTranspoProperties.offloadingTransporterDefaultEnabled;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (defaultTransportAgentToWorkerPrefix == null ? 0 : System.hashCode(defaultTransportAgentToWorkerPrefix));
        hashCode = (17 * hashCode) + (defaultTransportAgentToMasterPrefix == null ? 0 : System.hashCode(defaultTransportAgentToMasterPrefix));
        hashCode = (17 * hashCode) + (defaultTransportInputPackage == null ? 0 : System.hashCode(defaultTransportInputPackage));
        hashCode = (17 * hashCode) + (defaultTransportOutputPackage == null ? 0 : System.hashCode(defaultTransportOutputPackage));
        hashCode = (17 * hashCode) + (defaultTransportReplicationSynchronous == null ? 0 : System.hashCode(defaultTransportReplicationSynchronous));
        hashCode = (17 * hashCode) + (defaultTransportContentpackage == null ? 0 : System.hashCode(defaultTransportContentpackage));
        hashCode = (17 * hashCode) + (offloadingTransporterDefaultEnabled == null ? 0 : System.hashCode(offloadingTransporterDefaultEnabled));
        return hashCode;
    }
}

