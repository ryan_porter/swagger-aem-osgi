/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComAdobeGraniteWorkflowCoreJcrWor
 */
public class OASComAdobeGraniteWorkflowCoreJcrWor {
    /**
     * Get bucketSize
     * @return bucketSize
     */
    public OASConfigNodePropertyInteger bucketSize { get; set; }

    public static OASComAdobeGraniteWorkflowCoreJcrWor getExample() {
        OASComAdobeGraniteWorkflowCoreJcrWor comAdobeGraniteWorkflowCoreJcrWorkflowBucketManagerProperties = new OASComAdobeGraniteWorkflowCoreJcrWor();
          comAdobeGraniteWorkflowCoreJcrWorkflowBucketManagerProperties.bucketSize = OASConfigNodePropertyInteger.getExample();
        return comAdobeGraniteWorkflowCoreJcrWorkflowBucketManagerProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComAdobeGraniteWorkflowCoreJcrWor) {           
            OASComAdobeGraniteWorkflowCoreJcrWor comAdobeGraniteWorkflowCoreJcrWorkflowBucketManagerProperties = (OASComAdobeGraniteWorkflowCoreJcrWor) obj;
            return this.bucketSize == comAdobeGraniteWorkflowCoreJcrWorkflowBucketManagerProperties.bucketSize;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (bucketSize == null ? 0 : System.hashCode(bucketSize));
        return hashCode;
    }
}

