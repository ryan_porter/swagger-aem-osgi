/*
 * Adobe Experience Manager OSGI config (AEM) API
 * Swagger AEM OSGI is an OpenAPI specification for Adobe Experience Manager (AEM) OSGI Configurations API
 *
 * OpenAPI spec version: 1.0.0-pre.0
 * Contact: opensource@shinesolutions.com
 *
 * NOTE: This class is auto generated by the OAS code generator program.
 * https://github.com/OAS-api/OAS-codegen.git
 * Do not edit the class manually.
 */

/**
 * OASComDayCqReplicationImplReverseRep
 */
public class OASComDayCqReplicationImplReverseRep implements OAS.MappedProperties {
    /**
     * Get schedulerPeriod
     * @return schedulerPeriod
     */
    public OASConfigNodePropertyInteger schedulerPeriod { get; set; }

    private static final Map<String, String> propertyMappings = new Map<String, String>{
        'scheduler.period' => 'schedulerPeriod'
    };

    public Map<String, String> getPropertyMappings() {
        return propertyMappings;
    }

    public static OASComDayCqReplicationImplReverseRep getExample() {
        OASComDayCqReplicationImplReverseRep comDayCqReplicationImplReverseReplicatorProperties = new OASComDayCqReplicationImplReverseRep();
          comDayCqReplicationImplReverseReplicatorProperties.schedulerPeriod = OASConfigNodePropertyInteger.getExample();
        return comDayCqReplicationImplReverseReplicatorProperties;
    }

    public Boolean equals(Object obj) {
        if (obj instanceof OASComDayCqReplicationImplReverseRep) {           
            OASComDayCqReplicationImplReverseRep comDayCqReplicationImplReverseReplicatorProperties = (OASComDayCqReplicationImplReverseRep) obj;
            return this.schedulerPeriod == comDayCqReplicationImplReverseReplicatorProperties.schedulerPeriod;
        }
        return false;
    }

    public Integer hashCode() {
        Integer hashCode = 43;
        hashCode = (17 * hashCode) + (schedulerPeriod == null ? 0 : System.hashCode(schedulerPeriod));
        return hashCode;
    }
}

